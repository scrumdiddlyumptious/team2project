/**
 * 
 */

var imsiqueryinit = function() {
	$('#submitButtonSE01').click(
			function() {
				var startDate = $('#startDate').val();
				var endDate = $('#endDate').val();
				console.log(startDate);

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/IMSIByDateRange/"
						+ startDate + "&" + endDate,
						dataType : "json",
						async : false,
						success : renderListSE01
				});
				$(".imsiCallFailuresTableSE01").show();
			});


		$('#resetButtonSE01').click(function() {
			$('#startDate').val('');
			$('#endDate').val('');
			$('#imsiTableBodySE01').empty();
			
		});
		

		
	};
	
	
	

	var renderListSE01 = function(data) {
		var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
		$('#imsiTableBodySE01').empty();

		$.each(list, function(index, data) {
			var date = new Date(data[1]);
		
			var dateString = commonDateFormat(date);
			$('#imsiTableBodySE01').append(
					"<tr><td>" + data[0] +  "</td><td>" + dateString + "</td><td>" + data[2]
					+ "</td></tr>");
		});
		$('#imsiCallFailuresTableSE01').dataTable();

	};
	

/*	var loadQueryDataSE01 = function() {
		$('#imsiCallFailuresTableSE01').dataTable({
			"bAutoWidth": false,
			"bFilter": true,
			"sPaginationType": "full_numbers",
			"aoColumns": [
			              { "mData": "id" },
			              { "mData": "date", "mRender": function (data, type, full) {
		                       var date = new Date(data);
			 
		   					year = "" + date.getFullYear();
		   					month = "" + (date.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
		   					day = "" + date.getDate(); if (day.length == 1) { day = "0" + day; }
		   					hour = "" + date.getHours(); if (hour.length == 1) { hour = "0" + hour; }
		   					minute = "" + date.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
		   					second = "" + date.getSeconds(); if (second.length == 1) { second = "0" + second; }
		   					
		   					return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
		   					
		                    } },
						  { "mData": "imsi" },
			              { "mData": "date" },
			              { "mData": "failureClass"}
			            
			              ],
			              "bProcessing" : true,
			              "bServerSide" : true,
			              "sAjaxSource": "rest/dataset" 
			              "sAjaxSource" : "/IMSIByDateRange/{startDateString}&{endDateString}" 
					  				
		});
	};
	*/
