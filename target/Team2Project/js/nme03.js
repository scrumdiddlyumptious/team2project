/**
 * 
 */

var top10comboinit = function() {
	top10FailuresAccordionReset();
	
	$('#submitButtonNWE03').click(
			function() {
				var startDate = $('#startTimeNWE03').val();
				var endDate = $('#endTimeNWE03').val();

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/top10FailureCombos/"
							+ startDate + "&" + endDate,
					dataType : "json",
					async : false,
					success : renderComboList
				});
				$("#top10FailureCombos").show();
			});

	$('#resetButtonNWE03').click(function() {
		$('#startTimeNWE03').val('');
		$('#endTimeNWE03').val('');
		$('#tableBodyNWE03').empty();
		$('#graphButtonNWE03').remove();
	});

};

var top10FailuresAccordionReset = function() {
	$("#top10FailuresAccordion").accordion({ 
	
		active: false,
		collapsible: true,	
		heightStyle: "content",
		autoHeight: false,
		clearStyle: true,  
		
	});
};

var renderComboList = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#top10FailureCombosForm')
			.append(
					'<input type="button" id="graphButtonNWE03" class="button" value="Show Graph" />');

	$('#graphButtonNWE03').click(function() {
		displayComboGraph(list);
	});

	$('#tableBodyNWE03').empty();

	$.each(list, function(index, data) {
		$('#tableBodyNWE03').append(
				"<tr><td>" + data[0] + "</td><td>" + data[1] + "</td><td>"
						+ data[2] + "</td><td>" + data[3] + "</td></tr>");
	});
	$('#top10FailureCombos').dataTable({
		"order" : [ [ 4, "desc" ] ]
	});
};

function displayComboGraph(comboData) {
	$('#comboGraphDialog').remove();
	$('#top10FailureCombosSection').append('<div id="comboGraphDialog"></div>');
	var graphHTML = '<canvas id="comboChart" width="350" height="300"></canvas>';
	
	var frequencyData = [];
	
	$.each(comboData, function(index, data) {
		frequencyData[index] = data[3];
	});

	var barChartData = {
		labels : [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" ],
		datasets : [ {
			fillColor : "rgba(220,220,220,0.5)",
			strokeColor : "rgba(220,220,220,0.8)",
			highlightFill : "rgba(220,220,220,0.75)",
			highlightStroke : "rgba(220,220,220,1)",
			data : frequencyData
		} ]
	};

	$('#comboGraphDialog').dialog({
		autoOpen : true,
		modal : true,
		draggable : true,
		width : 630,
		height : 700,
		buttons : {
			"Close" : {
				text : 'Close',
				click : function() {
					$(this).dialog("close");
				}
			}
		},
		open : function() {
			$('#comboGraphDialog').html(graphHTML);
			// add chart
			var context = $('#comboChart').get(0).getContext("2d");
			new Chart(context).Bar(barChartData, {
				responsive : true
			});
		},
		Cancel : function() {
			$(this).dialog("close");
		}
	});
};