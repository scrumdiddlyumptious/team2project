
var rootUrl = "http://localhost:8080/Team2Project/rest/";

var initUploadForm = function() {
	$("#upload_btn").click(function() {
	
		$('#uploadMessage').empty();
		
		var fileIn = $("#fileToUpload")[0];
		// Has any file been selected yet?
		if (fileIn.files === undefined || fileIn.files.length == 0) {
			alert("Please select a file");
			return;
		}

		var file = fileIn.files[0];
		
		$.ajaxSetup({
			beforeSend: function() {
				$('#loading').html("Loading...");
			},
			complete: function() {
				$('#loading').empty();
			}
		});

		$.ajax({
			url : rootUrl + "dataset/upload?fileName=" + file.name + "&mimeType=" + file.type,
			type : "POST",
			data : file,
			processData : false,
			contentType : file.type,
			success : function(data) {
				showMessages(data);
			},
			error : function() {
				showMessages(data);
			}
		});
	});
};

var showMessages = function(data) {
	$.each(data, function(index, result) {
		$('#uploadMessage').append(data[index] + "<br>");
	});
};
