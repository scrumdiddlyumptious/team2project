/**
 * 
 */

var imsiCallFailuresQueryInit = function() {
	$('#submitButton').click(
			function() {
				var startTime = $('#startTime').val();
				var endTime = $('#endTime').val();

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/imsicallfailures/"
							+ startTime + "&" + endTime,
					dataType : "json",
					async : false,
					success : renderFailuresList
				});
			});
	
	$('#resetButton').click(function() {
		$('#startTime').val('');
		$('#endTime').val('');
		$('#imsiTableBody').empty();
	});
};

var renderFailuresList = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#imsiTableBodyNWE01').empty();
	$.each(list, function(index, data) {
		var startDate = new Date(data[1]);
		var endDate = new Date(data[2]);

		var startDateString = commonDateFormat(startDate);
			
		var endDateString =  commonDateFormat(endDate);
			
		
		$('#imsiTableBodyNWE01').append(
				"<tr><td>" + data[0] + "</td><td>" + startDateString + "</td><td>"
						+ endDateString + "</td><td>" + data[3] + "</td><td>"
						+ data[4] + "</td></tr>");
	});
	$('#imsiCallFailuresTable').dataTable();
};