
//$(function(){
	//$('.tab-admin03').on("click", function() {
var loadBaseData = function() {
	$('#baseDataTable').dataTable({
		"bAutoWidth": false,
		"bFilter": true,
		"sPaginationType": "full_numbers",
		"aoColumns": [
		              { "mData": "id" },
		              { "mData": "date", "mRender": function (data, type, full) {
	                       var date = new Date(data);
		 
//	   					year = "" + date.getFullYear();
//	   					month = "" + (date.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
//	   					day = "" + date.getDate(); if (day.length == 1) { day = "0" + day; }
//	   					hour = "" + date.getHours(); if (hour.length == 1) { hour = "0" + hour; }
//	   					minute = "" + date.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
//	   					second = "" + date.getSeconds(); if (second.length == 1) { second = "0" + second; }
	   					
	   					return commonDateFormat(date);
	   					
	                    } },
					  { "mData": "eventId" },
		              { "mData": "failureClass" },
		              { "mData": "userEquipment"},
		              { "mData": "market"},
		              { "mData": "operator"},
		              { "mData": "cellId"},
					  { "mData": "duration"},
					  { "mData": "causeCode"},
					  { "mData": "neVersion"},
					  { "mData": "imsi"},
					  { "mData": "hier3_ID"},
					  { "mData": "hier32_ID"},
					  { "mData": "hier321_ID"}
		              ],
		              "bProcessing" : true,
		              "bServerSide" : true,
		              "sAjaxSource": "rest/dataset"
					  				
	});
};
//});
//});
