/**
 * Val: Customer Service Rep Queries
 */
/* Story 4.1 */
var url="";
/*var imsiTitle = function(){
	var imsi = $('#imsiField').val();
	console.log(imsi + " created");
	imsiSearch(imsi);
};*/
var imsiSearch = function() {
	
	$( '#imsiField' ).autocomplete({
	      source: function( request, response ) {  
	    	  var imsi = $('#imsiField').val();
	        $.ajax({
	          url: "/Team2Project/rest/dataset/imsiAutocomplete/" + imsi ,
	          dataType: "json",
	          data: {
	            q: request.term
	          },
	          success: function( data ) {
	            response( data );
	          }
	        });
	      },
	      minLength: 7,  
	   });
	

	
	
	//4.2 IMSI + Failure search
	$("#dialog-imsi").dialog({
		autoOpen: false,
		title: "IMSI Failure Search",
		/*title: function(){
			return "IMSI: "+ $('#imsiField').val() +", Failure Search";
			},*/
		height: 280,
		width: 410,
		modal: true,
		buttons: {
			"Find": function() {
				var imsi = $('#imsiField').val();
				var imsiFailure = $('#imsiFailureSelect').val();
				var failureType = $('#imsiFailureSelect option:selected').text();
				console.log(imsi + ", " + imsiFailure + ", " + failureType);
				url="/imsifailures/"+ imsi + "&" + imsiFailure,
				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset" + url,
					dataType : "json",
					async : false,
					success : loadIMSIeventData
				});
				document.getElementById("imsiMessage").innerHTML = "<h2>Results for IMSI id: " + imsi
				+ " with failure type '" + failureType+ "' are:</h2>";
			}
		},
		Cancel: function() {
			$( this ).dialog( "close" );
		},
		close: function() {
			// allFields.val( "" ).removeClass( "ui-state-error" );
			console.log("closing imsifailure dialog box");
		}
	});
	//4.1 View all IMSIs
	$('#submitIMSIQuery').click(

			function() {
				var imsi = $('#imsiField').val();
				if(validateIMSI(imsi)){
					url="/imsifailures/"+ imsi,
					$.ajax({
						type : 'GET',
						url : "/Team2Project/rest/dataset" + url,
						dataType : "json",
						async : false,
						success : loadIMSIeventData
					});
					//document.getElementById("imsiMessage").innerHTML = "<h2>Results for IMSI id: " + imsi+"</h2>";
					//$(".imsiCSRtable").show();
				}
			});

	$('#submitIMSIQuery2').click(
			function() {
				console.log("imsifailure button pressed");
				if(validateIMSI($('#imsiField').val())){
					$( "#dialog-imsi" ).dialog( "open" );
				}
			});

	$('#resetIMSIButton').click(function() {
		$('#imsiField').val('');
		//$('#imsiCSRtable').empty();
		//$(".imsiCSRtable").hide();
	});
};

var loadIMSIeventData = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#imsi_table_body').empty();
	console.log(list.length);
	if(list.length==0){
		document.getElementById("imsiMessage").innerHTML = "<h2>There are no results for that IMSI with that search criteria.</h2>";
	}else{
		$.each(list, function(index, data) {
			$('#imsi_table_body').append(
					"<tr><td>" + data[0] + "</td><td>"
					+ data[1] + "</td><td>" + data[2] + "</td></tr>");
		});
		$('#imsiCSRtable').dataTable();
		$(".imsiCSRtable").show();

	}
};

var validateIMSI = function (imsi){
	document.getElementById("imsiMessage").innerHTML = "";
	hideCSRTables();
	console.log(imsi.length);
	var validated = false;
	if(isNaN(imsi)){
		document.getElementById("imsiMessage").innerHTML = "<h2>" + imsi +" is not a number.</h2>";
	}else if(imsi.length==0){
		document.getElementById("imsiMessage").innerHTML = "<h2>Enter IMSI number above.</h2>";
	}else if(imsi.length==15){
		validated = true;
		console.log("validated : " + validated);
		return validated;
	}else{
		document.getElementById("imsiMessage").innerHTML = "<h2>" + imsi +" is invalid.</h2>";
	}
	return validated;
};

var hideCSRTables = function (){
	$(".imsiCSRtable").hide();
}

/*var loadIMSIFailuretData = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#imsi_table_body').empty();

	$.each(list, function(index, data) {

		$('#imsi_table_body').append(
				"<tr><td>" + data[0] + "</td><td>"
				+ data[1] + "</td><td>" + data[2] + "</td></tr>");
	});
	$('#imsiCSRtable').dataTable();
};*/

/*
var loadIMSIeventData = function() {
	$('#imsiCSRtable').dataTable({
		"bAutoWidth": false,
		"bFilter": true,
		"sPaginationType": "full_numbers",
		"aoColumns": [
		              { "mData": "id" },
		             /* { "mData": "date", "mRender": function (data, type, full) {
	                       var date = new Date(data);

	   					year = "" + date.getFullYear();
	   					month = "" + (date.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
	   					day = "" + date.getDate(); if (day.length == 1) { day = "0" + day; }
	   					hour = "" + date.getHours(); if (hour.length == 1) { hour = "0" + hour; }
	   					minute = "" + date.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	   					second = "" + date.getSeconds(); if (second.length == 1) { second = "0" + second; }

	   					return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;

	                    } },*//*
					  { "mData": "eventId" },
					  { "mData": "causeCode"}
		              ],
		              "bProcessing" : true,
		              "bServerSide" : true,
		              "sAjaxSource": "rest/dataset"				
	});
};*/
