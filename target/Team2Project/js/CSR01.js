var imsiqueryinitCSR = function() {
	$('#submitButtonCSR01').click(
			function() {
				
				var imsi = $('#IMSI').val();
				var startDate = $('#startDateCSR01').val();
				var endDate = $('#endDateCSR01').val();
				console.log(startDate);

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/IMSIFailureCount/" + imsi + '&'
						+ startDate + "&" + endDate,
						dataType : "json",
						async : false,
						success : renderListCSR
				});
			});


		$('#resetButtonCSR01').click(function() {
			$('#IMSI').val('');
		
			$('#imsiTableBodyCSR01').empty();
		});
		
		
	};
	
	$(document).ready(function(){
		$("#dialog").dialog({
			autoOpen:false,
			width: 400,
			buttons:[]
		})
	});
			
	
	
	

	var renderListCSR = function(data) {
		var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
		$('#imsiTableBodyCSR01').empty();

		$.each(list, function(index, data) {

			$('#imsiTableBodyCSR01').append(
					"<tr><td>" + data[0] +  "</td><td>" + data[1]
					+ "</td></tr>");
		});
		

	};
