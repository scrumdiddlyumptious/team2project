var failureClassSearchIMSI = function() {
	$('#submitButtonSE03').click(
			
	function() {
		var imsiFailure = $('#imsiFailureRetrieve').val();
		url = "/getUniqueImsiForFailureClass/" + imsiFailure, 
		$.ajax({
			type : 'GET',
			url : "/Team2Project/rest/dataset" + url,
			dataType : "json",
			async : false,
			success : renderIMSIData
		});

	});
};

var renderIMSIData = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#imsiTableBodySE03').empty();
	$.each(list, function(index, data) {
		$('#imsiTableBodySE03').append("<tr><td>" + data + "</td></tr>");
	});
	$('#imsiCallFailuresTableSE03').dataTable();
	$(".imsiCSRtable").show();

};