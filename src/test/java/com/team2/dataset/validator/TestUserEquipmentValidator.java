package com.team2.dataset.validator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.junit.Before;
import org.junit.Test;

public class TestUserEquipmentValidator {
	UserEquipmentValidator testUserEquipmentValidator = new UserEquipmentValidator();
	HSSFCell tacCell, marketingNameCell, manufacturerCell, accessCapablityCell, modelCell, vendorNameCell, ueTypeCell, osCell, inputModeCell;
	@Before
	public void setUp() throws Exception {
		tacCell=mock(HSSFCell.class);
		marketingNameCell=mock(HSSFCell.class);
		manufacturerCell=mock(HSSFCell.class);
		accessCapablityCell=mock(HSSFCell.class);
		modelCell=mock(HSSFCell.class);
		vendorNameCell=mock(HSSFCell.class);
		ueTypeCell=mock(HSSFCell.class);
		osCell=mock(HSSFCell.class);
		inputModeCell=mock(HSSFCell.class);
		when(tacCell.getNumericCellValue()).thenReturn(200000.0);
		when(marketingNameCell.getCellType()).thenReturn(1);
		when(manufacturerCell.getCellType()).thenReturn(1);
		when(accessCapablityCell.getCellType()).thenReturn(1);
		when(modelCell.getCellType()).thenReturn(1);
		when(vendorNameCell.getCellType()).thenReturn(1);
		when(ueTypeCell.getCellType()).thenReturn(1);
		when(osCell.getCellType()).thenReturn(1);
		when(inputModeCell.getCellType()).thenReturn(1);

		
	}

	@Test
	public void testValidUeObject() {
		HSSFCell[] ueTableObject ={ tacCell,marketingNameCell, manufacturerCell,
				accessCapablityCell, modelCell, vendorNameCell, ueTypeCell, osCell, inputModeCell};
		assertTrue("Valid User Equipment Object",testUserEquipmentValidator.validate(ueTableObject) );
		
	}
	@Test
	public void invalidtacCell() {
		when(tacCell.getNumericCellValue()).thenReturn(40000001.0);
		HSSFCell[] ueTableObject ={ tacCell,marketingNameCell, manufacturerCell,
				accessCapablityCell, modelCell, vendorNameCell, ueTypeCell, osCell, inputModeCell};
		assertFalse("Invalid due to TacCell",testUserEquipmentValidator.validate(ueTableObject) );
		
	}
	@Test
	public void invalidStringCell() {
		
		HSSFCell[] ueTableObject ={ tacCell,marketingNameCell, manufacturerCell,
				accessCapablityCell, modelCell, vendorNameCell, ueTypeCell, osCell, inputModeCell};
		for(int i =1;i<ueTableObject.length;i++){
			when(ueTableObject[i].getCellType()).thenReturn(2);
			assertFalse("Invalid due to incorrect CellType",testUserEquipmentValidator.validate(ueTableObject) );
		}
	}
	
			

}
