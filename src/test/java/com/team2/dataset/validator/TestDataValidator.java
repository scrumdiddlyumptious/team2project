package com.team2.dataset.validator;

import static org.junit.Assert.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;

import static org.mockito.Mockito.when;



public class TestDataValidator {
	
	private HSSFCell[] cellArray;

	@Before
	public void setUp() throws Exception {
	
		cellArray = new HSSFCell[14];
		for(int i =0;i<14;i++){
			cellArray[i]= mock(HSSFCell.class);
			when(cellArray[i].toString()).thenReturn("(null)");
		}
	}

	@Test
	public void testValidateDataMethodForBaseData() {
		assertEquals(DataValidator.validateData(cellArray, "Base Data"),false);
	}
	
	@Test 
	public void testValidateDataMethodForEventCause() {
		assertEquals(DataValidator.validateData(cellArray, "Event-Cause Table"),false);
	}
	
	@Test
	public void testValidateDataMethodForFailureClassTable() {
		assertEquals(DataValidator.validateData(cellArray, "Failure Class Table"),false);
	}
	
	@Test
	public void testValidateDataMethodForMCC_MNCTable() {
		assertEquals(DataValidator.validateData(cellArray, "MCC - MNC Table"),false);
	}
	
	@Test
	public void testValidateDataMethodForuETable() {
		assertEquals(DataValidator.validateData(cellArray, "UE Table"),false);
	}



}
