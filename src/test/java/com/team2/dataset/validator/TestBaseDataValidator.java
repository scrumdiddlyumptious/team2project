package com.team2.dataset.validator;

import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.junit.Before;
import org.junit.Test;

import com.team2.cache.Cache;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import static org.mockito.Mockito.when;

public class TestBaseDataValidator {
	BaseDataValidator testBasedataValidator = new BaseDataValidator();
	HSSFCell dateCell, eventIdCell, failureClassCell, ueTypeCell, marketCell,
			operatorCell, cellIdCell, durationCell, causeCodeCell,
			neVersionCell, imsiCell, hier3_IDCell, hier32_IDCell,
			hier321_IDCell;

	@Before
	public void setUp() throws Exception {
		Cache.setFileName("AIT Group Project - Dataset 3A.xls");
		Cache.initializeCache();
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		CellStyle cellStyle = workbook.createCellStyle();
		CreationHelper createHelper = workbook.getCreationHelper();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(
				"m/d/yy h:mm"));
		dateCell = sheet.createRow(0).createCell(0);
		Calendar cal = Calendar.getInstance();
		cal.set(1900, 0, 1);
		Date valueToTest = cal.getTime();
		dateCell.setCellValue(valueToTest);
		dateCell.setCellStyle(cellStyle);
		eventIdCell = mock(HSSFCell.class);
		failureClassCell = mock(HSSFCell.class);
		ueTypeCell = mock(HSSFCell.class);
		marketCell = mock(HSSFCell.class);
		operatorCell = mock(HSSFCell.class);
		cellIdCell = mock(HSSFCell.class);
		durationCell = mock(HSSFCell.class);
		causeCodeCell = mock(HSSFCell.class);
		neVersionCell = mock(HSSFCell.class);
		imsiCell = mock(HSSFCell.class);
		hier3_IDCell = mock(HSSFCell.class);
		hier32_IDCell = mock(HSSFCell.class);
		hier321_IDCell = mock(HSSFCell.class);
		when(eventIdCell.getNumericCellValue()).thenReturn(4098.0);
		when(failureClassCell.getNumericCellValue()).thenReturn(1.0);
		when(ueTypeCell.getNumericCellValue()).thenReturn(33001735.0);
		when(marketCell.getNumericCellValue()).thenReturn(405.0);
		when(operatorCell.getNumericCellValue()).thenReturn(1.0);
		when(cellIdCell.getNumericCellValue()).thenReturn(3.0);
		when(durationCell.getNumericCellValue()).thenReturn(1000.0);
		when(causeCodeCell.getNumericCellValue()).thenReturn(1.0);
		when(neVersionCell.getStringCellValue()).thenReturn("13A");
		when(imsiCell.getStringCellValue()).thenReturn("191911000312238");
		when(hier3_IDCell.getStringCellValue()).thenReturn(
				"5990040447850810000");
		when(hier32_IDCell.getStringCellValue()).thenReturn(
				"4173989014826130000");
		when(hier321_IDCell.getStringCellValue()).thenReturn(
				"6423140930350090000");

	}

	@Test
	public void testValidBaseDataObject() {
		HSSFCell[] baseDataObject = { dateCell, eventIdCell, failureClassCell,
				ueTypeCell, marketCell, operatorCell, cellIdCell, durationCell,
				causeCodeCell, neVersionCell, imsiCell, hier3_IDCell,
				hier32_IDCell, hier321_IDCell };

		assertTrue("Base Data Object is Invalid",
				testBasedataValidator.validate(baseDataObject));

	}

	@Test
	public void testForNullValue() {
		when(cellIdCell.toString()).thenReturn("(null)");

		HSSFCell[] baseDataObject = { dateCell, eventIdCell, failureClassCell,
				ueTypeCell, marketCell, operatorCell, cellIdCell, durationCell,
				causeCodeCell, neVersionCell, imsiCell, hier3_IDCell,
				hier32_IDCell, hier321_IDCell };
		assertFalse("Invalid due to null value",
				testBasedataValidator.validate(baseDataObject));
	}

	@Test
	public void testForInvalidDateObject() {
		dateCell = mock(HSSFCell.class);

		HSSFCell[] baseDataObject = { dateCell, eventIdCell, failureClassCell,
				ueTypeCell, marketCell, operatorCell, cellIdCell, durationCell,
				causeCodeCell, neVersionCell, imsiCell, hier3_IDCell,
				hier32_IDCell, hier321_IDCell };
		assertFalse("Invalid due to improperly formatted dateCell value",
				testBasedataValidator.validate(baseDataObject));
	}

	@Test
	public void testForInvalidEventCauseInfo() {
		when(eventIdCell.getNumericCellValue()).thenReturn(4099.0);

		HSSFCell[] baseDataObject = { dateCell, eventIdCell, failureClassCell,
				ueTypeCell, marketCell, operatorCell, cellIdCell, durationCell,
				causeCodeCell, neVersionCell, imsiCell, hier3_IDCell,
				hier32_IDCell, hier321_IDCell };
		assertFalse(
				"Invalid due to invalid combination of eventID and causeCode",
				testBasedataValidator.validate(baseDataObject));
	}

	@Test
	public void testForInvalidMccMncInfo() {
		when(operatorCell.getNumericCellValue()).thenReturn(6.0);

		HSSFCell[] baseDataObject = { dateCell, eventIdCell, failureClassCell,
				ueTypeCell, marketCell, operatorCell, cellIdCell, durationCell,
				causeCodeCell, neVersionCell, imsiCell, hier3_IDCell,
				hier32_IDCell, hier321_IDCell };
		assertFalse("Invalid due to invalid combination of mcc and mnc number",
				testBasedataValidator.validate(baseDataObject));
	}

	@Test
	public void testForInvalidUeType() {
		when(ueTypeCell.getNumericCellValue()).thenReturn(33000000.0);

		HSSFCell[] baseDataObject = { dateCell, eventIdCell, failureClassCell,
				ueTypeCell, marketCell, operatorCell, cellIdCell, durationCell,
				causeCodeCell, neVersionCell, imsiCell, hier3_IDCell,
				hier32_IDCell, hier321_IDCell };
		assertFalse("Invalid due to invalid ueType",
				testBasedataValidator.validate(baseDataObject));
	}

}
