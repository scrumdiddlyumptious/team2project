package com.team2.dataset.validator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.junit.Before;
import org.junit.Test;

public class TestMCC_MNCValidator {

	MCC_MNCValidator testMCC_MNCValidator = new MCC_MNCValidator();
	HSSFCell mccCell, mncCell, countryCell, operatorCell;

		@Before
		public void setUp() throws Exception {
			mccCell = mock(HSSFCell.class);
			mncCell=mock(HSSFCell.class);
			countryCell=mock(HSSFCell.class);
			operatorCell=mock(HSSFCell.class);
			when(mccCell.getNumericCellValue()).thenReturn(238.0);
			when(mncCell.getNumericCellValue()).thenReturn(3.0);
			when(countryCell.getCellType()).thenReturn(1);
			when(operatorCell.getCellType()).thenReturn(1);
		}

		@Test
		public void testValidMccMncObject() {
			HSSFCell[] mccMncObject ={mccCell,mncCell,countryCell,operatorCell };
			assertTrue("Valid MCC_MNC Class", testMCC_MNCValidator.validate(mccMncObject));
		}
		@Test
		public void testNullValueObject() {
			when(countryCell.toString()).thenReturn("(null)");
			HSSFCell[] mccMncObject ={mccCell,mncCell,countryCell,operatorCell };
			assertFalse("Invalid due to null value", testMCC_MNCValidator.validate(mccMncObject));
		}
		@Test
		public void testInvalidMccell() {
			when(mccCell.getNumericCellValue()).thenReturn(601.0);
			HSSFCell[] mccMncObject ={mccCell,mncCell,countryCell,operatorCell };
			assertFalse("Invalid due to mccCell value", testMCC_MNCValidator.validate(mccMncObject));
		}
		@Test
		public void testInvalidMncCell() {
			when(mncCell.getNumericCellValue()).thenReturn(1001.0);
			HSSFCell[] mccMncObject ={mccCell,mncCell,countryCell,operatorCell };
			assertFalse("Invalid due to mncCell value", testMCC_MNCValidator.validate(mccMncObject));
		}
		@Test
		public void testInvalidCountry() {
			when(countryCell.getCellType()).thenReturn(2);
			HSSFCell[] mccMncObject ={mccCell,mncCell,countryCell,operatorCell };
			assertFalse("Invalid due to countryCell value", testMCC_MNCValidator.validate(mccMncObject));
		}
		@Test
		public void testInvalidOperator() {
			when(operatorCell.getCellType()).thenReturn(2);
			HSSFCell[] mccMncObject ={mccCell,mncCell,countryCell,operatorCell };
			assertFalse("Invalid due to operatorCell value", testMCC_MNCValidator.validate(mccMncObject));
		}


}
