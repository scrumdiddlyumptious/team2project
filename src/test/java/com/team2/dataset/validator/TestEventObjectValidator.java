package com.team2.dataset.validator;

import static org.junit.Assert.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
public class TestEventObjectValidator {
EventObjectValidator testEventObjectValidator = new EventObjectValidator();
HSSFCell causeCodeCell, eventIdCell, descriptionCell;

	@Before
	public void setUp() throws Exception {
		causeCodeCell = mock(HSSFCell.class);
		eventIdCell=mock(HSSFCell.class);
		descriptionCell=mock(HSSFCell.class);
		when(causeCodeCell.getNumericCellValue()).thenReturn(1.0);
		when(eventIdCell.getNumericCellValue()).thenReturn(4097.0);
		when(descriptionCell.getCellType()).thenReturn(1);
	}

	@Test
	public void testValidEventCauseObject() {
		HSSFCell[] eventCauseObject ={causeCodeCell,eventIdCell,descriptionCell};
		assertTrue("Valid Event Cause Class", testEventObjectValidator.validate(eventCauseObject));
	}
	@Test
	public void testNullValueObject() {
		when(descriptionCell.toString()).thenReturn("(null)");
		HSSFCell[] eventCauseObject ={causeCodeCell,eventIdCell,descriptionCell};
		assertFalse("Invalid due to null value", testEventObjectValidator.validate(eventCauseObject));
	}
	@Test
	public void testInvalidCauseCodeCell() {
		when(causeCodeCell.getNumericCellValue()).thenReturn(34.0);
		HSSFCell[] eventCauseObject ={causeCodeCell,eventIdCell,descriptionCell};
		assertFalse("Invalid due to cause Code", testEventObjectValidator.validate(eventCauseObject));
	}
	@Test
	public void testInvalidEventIdCell() {
		when(eventIdCell.getNumericCellValue()).thenReturn(-4001.0);
		HSSFCell[] eventCauseObject ={causeCodeCell,eventIdCell,descriptionCell};
		assertFalse("Invalid due to cause Code", testEventObjectValidator.validate(eventCauseObject));
	}
	@Test
	public void testInvalidDescription() {
		when(descriptionCell.getCellType()).thenReturn(2);
		HSSFCell[] eventCauseObject ={causeCodeCell,eventIdCell,descriptionCell};
		assertFalse("Invalid due to description", testEventObjectValidator.validate(eventCauseObject));
	}

}
