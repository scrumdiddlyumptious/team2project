package com.team2.dataset.validator;

import static org.junit.Assert.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
public class TestFailureClassValidator {
	FailureClassValidator testFailureClassValidator = new FailureClassValidator();
	HSSFCell failureClassCell, descriptionCell;

	@Before
	public void setUp() throws Exception {
		failureClassCell = mock(HSSFCell.class);
		descriptionCell=mock(HSSFCell.class);
		when(failureClassCell.getNumericCellValue()).thenReturn(1.0);
		when(descriptionCell.getCellType()).thenReturn(1);
	}

	@Test
	public void testValidFailureClass() {
		HSSFCell[] failureClassObject ={failureClassCell,descriptionCell};
		assertTrue("Valid failure Class", testFailureClassValidator.validate(failureClassObject));
		
	}
	
	@Test
	public void testWhenNullValueIsPassed(){
		when(failureClassCell.toString()).thenReturn("(null)");
		HSSFCell[] failureClassObject ={failureClassCell,descriptionCell};
		assertFalse("Invalid failure Class due to null", testFailureClassValidator.validate(failureClassObject));

	}
	@Test
	public void testInvalidFailureClassValue(){
		when(failureClassCell.getNumericCellValue()).thenReturn(5.0);
		HSSFCell[] failureClassObject ={failureClassCell,descriptionCell};
		assertFalse("Invalid FailureClass value", testFailureClassValidator.validate(failureClassObject));

	}
	
	@Test
	public void testInvalidDescriptionCell(){
		when(descriptionCell.getCellType()).thenReturn(2);
		HSSFCell[] failureClassObject ={failureClassCell,descriptionCell};
		assertFalse("Invalid description type", testFailureClassValidator.validate(failureClassObject));

	}

}
