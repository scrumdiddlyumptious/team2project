package com.team2.dataset.businesslogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.team2.dataset.entities.BaseData;
import com.team2.dataset.entities.EventCause;
import com.team2.dataset.entities.FailureClass;
import com.team2.dataset.entities.MCC_MNC;
import com.team2.dataset.entities.UserEquipment;

public class TestDatasetDAO {

	private DatasetDAO datasetDAO = new DatasetDAO();

	@Before
	public void injectMockEntityManager() throws Exception {
		EntityManager entityManager = mock(EntityManager.class);
		datasetDAO.setEntityManager(entityManager);
	}

	@Test
	public void testEntityManager() throws Exception {
		assertNotNull(datasetDAO.getEntityManager());
	}

	@Test
	public void testGetBaseDataCount() throws Exception {
		Query query = mock(Query.class);
		List<BaseData> resultList = new ArrayList<BaseData>();

		when(datasetDAO.getEntityManager().createNamedQuery("getAllBaseData"))
				.thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getBaseDataCount(), 0);
	}

	@Test
	public void testGetAllBaseDataNoSearchTerm() {
		int start = 0;
		int amount = 0;
		String colName = "";
		String searchTerm = "";
		String sortDir = "";

		Query query = mock(Query.class);
		String sql = "SELECT * FROM BaseData ORDER BY " + colName + " "
				+ sortDir + " limit " + start + "," + amount;
		List<BaseData> resultList = new ArrayList<BaseData>();

		when(
				datasetDAO.getEntityManager().createNativeQuery(sql,
						BaseData.class)).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(
				datasetDAO.getAllBaseData(start, amount, colName, searchTerm,
						sortDir).size(), 0);
	}

	@Test
	public void testGetAllBaseDataWithSearchTerm() {
		int start = 0;
		int amount = 0;
		String colName = "";
		String searchTerm = "test";
		String sortDir = "";
		String globeSearch = " where (id like '%" + searchTerm + "%'"
				+ " or date like '%" + searchTerm + "%'"
				+ " or eventId like '%" + searchTerm + "%'"
				+ " or failureClass like '%" + searchTerm + "%'"
				+ " or userEquipment like '%" + searchTerm + "%'"
				+ " or market like '%" + searchTerm + "%'"
				+ " or operator like '%" + searchTerm + "%'"
				+ " or duration like '%" + searchTerm + "%'"
				+ " or causeCode like '%" + searchTerm + "%'"
				+ " or neVersion like '%" + searchTerm + "%'"
				+ " or imsi like '%" + searchTerm + "%'"
				+ " or hier3_ID like '%" + searchTerm + "%'"
				+ " or hier32_ID like '%" + searchTerm + "%'"
				+ " or hier321_ID like '%" + searchTerm + "%')";

		Query query = mock(Query.class);
		String sql = "SELECT * FROM BaseData " + globeSearch + " ORDER BY "
				+ colName + " " + sortDir + " limit " + start + "," + amount;
		List<BaseData> resultList = new ArrayList<BaseData>();

		when(
				datasetDAO.getEntityManager().createNativeQuery(sql,
						BaseData.class)).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(
				datasetDAO.getAllBaseData(start, amount, colName, searchTerm,
						sortDir).size(), 0);
	}

	@Test
	public void testGetIMSICallFailures() throws Exception {
		Query query = mock(Query.class);
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date startTime = formatter.parse("2013-01-11 17:15:00");
		Date endTime = formatter.parse("2013-01-11 17:20:00");

		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"getIMSICallFailures")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getIMSICallFailures(startTime, endTime).size(),
				0);
	}
	//////////////////////
	@Test
	public void testIMSIFailureByDate() throws Exception {
		Query query = mock(Query.class);
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date startTime = formatter.parse("2013-01-11 17:15:00");
		Date endTime = formatter.parse("2013-01-11 17:20:00");

		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"findIMSIByDateRange")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getIMSIByDateRange(startTime, endTime).size(),
				0);
	}

	/////////////////////
	@Test
	public void testGetTopTenIMSIFailures() throws Exception {
		Query query = mock(Query.class);
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date startTime = formatter.parse("2013-01-11 17:15:00");
		Date endTime = formatter.parse("2013-01-11 17:20:00");

		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"getTopTenIMSIFailures")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getTopTenIMSIFailures(startTime, endTime).size(),
				0);
	}
	
	@Test
	public void testGetTopTenCombos() throws Exception {
		Query query = mock(Query.class);
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date startTime = formatter.parse("2013-01-11 17:15:00");
		Date endTime = formatter.parse("2013-01-11 17:20:00");

		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"findTop10FailureCombos")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getTop10FailureCombos(startTime, endTime).size(),
				0);
	}

	@Test
	public void testAddBaseData() throws Exception {
		BaseData baseData = new BaseData();
		baseData.setId(1);
		datasetDAO.addData(baseData);

		ArgumentCaptor<BaseData> addedBaseData = ArgumentCaptor
				.forClass(BaseData.class);
		verify(datasetDAO.getEntityManager(), atLeastOnce()).merge(
				addedBaseData.capture());

		int id = addedBaseData.getValue().getId();
		assertEquals(1, id);
	}

	@Test
	public void testAddMCC_MNC() throws Exception {
		MCC_MNC mcc_mncData = new MCC_MNC();
		mcc_mncData.setMobileCountryCode(1234);
		datasetDAO.addData(mcc_mncData);

		ArgumentCaptor<MCC_MNC> addedMCC_MNC = ArgumentCaptor
				.forClass(MCC_MNC.class);
		verify(datasetDAO.getEntityManager(), atLeastOnce()).merge(
				addedMCC_MNC.capture());

		int mcc = addedMCC_MNC.getValue().getMobileCountryCode();
		assertEquals(1234, mcc);
	}

	@Test
	public void testAddFailureClass() throws Exception {
		FailureClass failureClass = new FailureClass();
		failureClass.setFailureClassID(1);
		datasetDAO.addData(failureClass);

		ArgumentCaptor<FailureClass> addedFailureClass = ArgumentCaptor
				.forClass(FailureClass.class);
		verify(datasetDAO.getEntityManager(), atLeastOnce()).merge(
				addedFailureClass.capture());

		int failureClassID = addedFailureClass.getValue().getFailureClassID();
		assertEquals(1, failureClassID);
	}

	@Test
	public void testAddUserEquipment() throws Exception {
		UserEquipment userEquipment = new UserEquipment();
		userEquipment.setTypeAllocationCode(1);
		datasetDAO.addData(userEquipment);

		ArgumentCaptor<UserEquipment> addedUserEquipment = ArgumentCaptor
				.forClass(UserEquipment.class);
		verify(datasetDAO.getEntityManager(), atLeastOnce()).merge(
				addedUserEquipment.capture());

		int typeAllocationCode = addedUserEquipment.getValue()
				.getTypeAllocationCode();
		assertEquals(1, typeAllocationCode);
	}

	@Test
	public void testAddEventCause() throws Exception {
		EventCause eventCause = new EventCause();
		eventCause.setCauseCode(1);
		datasetDAO.addData(eventCause);

		ArgumentCaptor<EventCause> addedEventCause = ArgumentCaptor
				.forClass(EventCause.class);
		verify(datasetDAO.getEntityManager(), atLeastOnce()).merge(
				addedEventCause.capture());

		int causeCode = addedEventCause.getValue().getCauseCode();
		assertEquals(1, causeCode);
	}
	
//	@Test
//	public void testGetImsiCallFailures(){
//		List<BaseData> resultList = new ArrayList<BaseData>();
//		Query query = mock(Query.class);
//
//		when(
//				datasetDAO.getEntityManager().createNamedQuery(
//						"getIMSIFailures")).thenReturn(query);
//		when(query.getResultList()).thenReturn(resultList);
//
//		assertEquals(datasetDAO. getIMSIFailures("191911000456426").size(),
//				0);
//	}
//	
//	
//	@Test
//	public void testImsiAutocomplete(){
//		List<BaseData> resultList = new ArrayList<BaseData>();
//		Query query = mock(Query.class);
//
//		when(
//				datasetDAO.getEntityManager().createNamedQuery(
//						"imsiAutocomplete")).thenReturn(query);
//		when(query.getResultList()).thenReturn(resultList);
//
//		assertEquals(datasetDAO.imsiAutocomplete("191").size(),
//				0);
//	}
	@Test
	public void testGetManufacturers(){
		List<BaseData> resultList = new ArrayList<BaseData>();
		Query query = mock(Query.class);
		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"getManufacturers")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getManufacturers().size(),
				0);
		
	}
	
	@Test
	public void testGetMarketingName(){
		List<BaseData> resultList = new ArrayList<BaseData>();
		Query query = mock(Query.class);
		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"getMarketingName")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getMarketingName("Mitsubishi").size(),
				0);
		
	}
	
	@Test
	public void testGetFailureCountByModel() throws ParseException{
			Query query = mock(Query.class);
			List<BaseData> resultList = new ArrayList<BaseData>();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date startTime = formatter.parse("2013-01-11 17:15:00");
			Date endTime = formatter.parse("2013-01-11 17:20:00");

			when(
					datasetDAO.getEntityManager().createNamedQuery(
							"getFailureCountByModel")).thenReturn(query);
			when(query.getResultList()).thenReturn(resultList);

			assertEquals(datasetDAO.getFailureCountByModel("Mitsubishi",startTime, endTime).size(),
					0);
	}
	
	@Test
	public void testGetUniqueEventId(){
		List<BaseData> resultList = new ArrayList<BaseData>();
		Query query = mock(Query.class);
		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"getUniqueEventId")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getUniqueEventId("G410","Mitsubishi").size(),
				0);
	}
	
	@Test
	public void testGetUniqueEventIdCauseCode(){
		List<BaseData> resultList = new ArrayList<BaseData>();
		Query query = mock(Query.class);
		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"getUniqueEventIdCauseCode")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getUniqueEventIdCauseCode("G410","Mitsubishi").size(),
				0);
	}
	
	@Test
	public void testGetCountUniqueEventIdCauseCode(){
		List<BaseData> resultList = new ArrayList<BaseData>();
		Query query = mock(Query.class);
		when(
				datasetDAO.getEntityManager().createNamedQuery(
						"getCountUniqueEventIdCauseCode")).thenReturn(query);
		when(query.getResultList()).thenReturn(resultList);

		assertEquals(datasetDAO.getCountUniqueEventIdCauseCode("G410","Mitsubishi").size(),
				0);
	}
	
	
}
