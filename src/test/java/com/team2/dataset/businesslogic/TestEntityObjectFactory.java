package com.team2.dataset.businesslogic;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.junit.Before;
import org.junit.Test;

import com.team2.cache.Cache;
import com.team2.dataset.entities.BaseData;

public class TestEntityObjectFactory {
	private HSSFCell dateCell, eventIdCell, failureClassCell, ueTypeCell,
			marketCell, operatorCell, cellIdCell, durationCell, causeCodeCell,
			neVersionCell, imsiCell, hier3_IDCell, hier32_IDCell,
			hier321_IDCell;
	private HSSFCell[] baseDataObject;
	private EntityObjectFactory entityObjectFactory= new EntityObjectFactory();

	@Before
	public void setUp() throws Exception {
	
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		CellStyle cellStyle = workbook.createCellStyle();
		CreationHelper createHelper = workbook.getCreationHelper();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(
				"m/d/yy h:mm"));
		dateCell = sheet.createRow(0).createCell(0);
		Calendar cal = Calendar.getInstance();
		cal.set(1900, 0, 1);
		Date valueToTest = cal.getTime();
		dateCell.setCellValue(valueToTest);
		dateCell.setCellStyle(cellStyle);
		eventIdCell = mock(HSSFCell.class);
		failureClassCell = mock(HSSFCell.class);
		ueTypeCell = mock(HSSFCell.class);
		marketCell = mock(HSSFCell.class);
		operatorCell = mock(HSSFCell.class);
		cellIdCell = mock(HSSFCell.class);
		durationCell = mock(HSSFCell.class);
		causeCodeCell = mock(HSSFCell.class);
		neVersionCell = mock(HSSFCell.class);
		imsiCell = mock(HSSFCell.class);
		hier3_IDCell = mock(HSSFCell.class);
		hier32_IDCell = mock(HSSFCell.class);
		hier321_IDCell = mock(HSSFCell.class);
	
		when(eventIdCell.getNumericCellValue()).thenReturn(4098.0);
		when(failureClassCell.getNumericCellValue()).thenReturn(1.0);
		when(ueTypeCell.getNumericCellValue()).thenReturn(33001735.0);
		when(marketCell.getNumericCellValue()).thenReturn(405.0);
		when(operatorCell.getNumericCellValue()).thenReturn(1.0);
		when(cellIdCell.getNumericCellValue()).thenReturn(3.0);
		when(durationCell.getNumericCellValue()).thenReturn(1000.0);
		when(causeCodeCell.getNumericCellValue()).thenReturn(1.0);
		when(neVersionCell.toString()).thenReturn("13A");
		when(imsiCell.getStringCellValue()).thenReturn("191911000312238");
		when(hier3_IDCell.getStringCellValue()).thenReturn(
				"5990040447850810000");
		when(hier32_IDCell.getStringCellValue()).thenReturn(
				"4173989014826130000");
		when(hier321_IDCell.getStringCellValue()).thenReturn(
				"6423140930350090000");
		HSSFCell[] baseDataObject = { dateCell, eventIdCell, failureClassCell,
				ueTypeCell, marketCell, operatorCell, cellIdCell, durationCell,
				causeCodeCell, neVersionCell, imsiCell, hier3_IDCell,
				hier32_IDCell, hier321_IDCell };
		
		
		
		this.baseDataObject = baseDataObject;
	
		
		entityObjectFactory.createEntityObject(baseDataObject, "Base Data");
	}

	@Test
	public void testCreateBaseDataMethod() {
		assertEquals(entityObjectFactory.createEntityObject(baseDataObject, "Base Data") instanceof BaseData,true);
	}

}
