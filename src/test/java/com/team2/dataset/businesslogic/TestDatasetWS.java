package com.team2.dataset.businesslogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.team2.dataset.entities.BaseData;

public class TestDatasetWS {

	private DatasetWS datasetWS = new DatasetWS();

	@Before
	public void injectMockDatasetDAO() throws Exception {
		DatasetDAO datasetDAO = mock(DatasetDAO.class);
		datasetWS.setDataPersistorDAO(datasetDAO);
	}

	@Test
	public void testFindAll() throws Exception {
		List<BaseData> resultList = new ArrayList<BaseData>();
		int start = 0;
		int amount = 0;
		String colName = "0";
		String searchTerm = "";
		String sortDir = "";

		when(
				datasetWS.getDataPersistorDAO().getAllBaseData(start, amount,
						colName, searchTerm, sortDir)).thenReturn(resultList);
		assertNotNull(datasetWS.findAll(0, amount, start, searchTerm, colName,
				"", sortDir));
	}

	@Test
	public void testGetIMSICallFailures() throws Exception {
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String startTimeString = "2013-01-11T17:10";
		String endTimeString = "2013-01-11T17:50";
		Date startTimeIn = inFormat.parse(startTimeString);
		String startTimeOut = outFormat.format(startTimeIn);

		Date endTimeIn = inFormat.parse(endTimeString);
		String endTimeOut = outFormat.format(endTimeIn);

		Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.parse(startTimeOut);

		Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.parse(endTimeOut);

		when(
				datasetWS.getDataPersistorDAO().getIMSICallFailures(startTime,
						endTime)).thenReturn(resultList);
		assertEquals(
				datasetWS.getIMSICallFailures(startTimeString, endTimeString)
						.size(), 0);
	}

	@Test(expected = ParseException.class)
	public void testGetIMSICallFailuresDateFormatException() throws Exception {
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		String startTimeString = "2013-01-AA 17:15:00";
		String endTimeString = "2013-01-AA 17:15:30";
		Date startTime = formatter.parse(startTimeString);
		Date endTime = formatter.parse(endTimeString);

		when(
				datasetWS.getDataPersistorDAO().getIMSICallFailures(startTime,
						endTime)).thenReturn(resultList);
		assertEquals(
				datasetWS.getIMSICallFailures(startTimeString, endTimeString)
						.size(), 0);
	}
	
	@Test
	public void testGetTopTenIMSIFailures() throws Exception {
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String startTimeString = "2013-01-11T17:10";
		String endTimeString = "2013-01-11T17:50";
		Date startTimeIn = inFormat.parse(startTimeString);
		String startTimeOut = outFormat.format(startTimeIn);

		Date endTimeIn = inFormat.parse(endTimeString);
		String endTimeOut = outFormat.format(endTimeIn);

		Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.parse(startTimeOut);

		Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.parse(endTimeOut);

		when(
				datasetWS.getDataPersistorDAO().getTopTenIMSIFailures(startTime,
						endTime)).thenReturn(resultList);
		assertEquals(
				datasetWS.getTopTenIMSIFailures(startTimeString, endTimeString)
						.size(), 0);
	}

	@Test
	public void testGetTopTenCombos() throws Exception {
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String startTimeString = "2013-01-11T17:10";
		String endTimeString = "2013-01-11T17:50";
		Date startTimeIn = inFormat.parse(startTimeString);
		String startTimeOut = outFormat.format(startTimeIn);

		Date endTimeIn = inFormat.parse(endTimeString);
		String endTimeOut = outFormat.format(endTimeIn);

		Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.parse(startTimeOut);

		Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.parse(endTimeOut);

		when(
				datasetWS.getDataPersistorDAO().getTop10FailureCombos(startTime,
						endTime)).thenReturn(resultList);
		assertEquals(
				datasetWS.getTop10FailureCombos(startTimeString, endTimeString)
						.size(), 0);
	}
	
	@Test
	public void testGetImsiByFailure(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getUniqueImsiForFailureClass(2)).thenReturn(resultList);
		assertEquals(
				datasetWS.getUniqueImsiForFailureClass(2)
						.size(), 0);

		
	
	}
	
	@Test
	public void testGetIMSIFailureCauseCode(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getIMSIFailureCauseCodes("191911000002886")).thenReturn(resultList);
		assertEquals(
				datasetWS.getIMSIFailureCauseCodes("191911000002886")
						.size(), 0);

	}
	

	


	
	@Test
	public void testIMSIFailureByDate() throws Exception {
		List<BaseData> resultList = new ArrayList<BaseData>();
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String startTimeString = "2013-01-11T17:10";
		String endTimeString = "2013-01-11T17:50";
		Date startTimeIn = inFormat.parse(startTimeString);
		String startTimeOut = outFormat.format(startTimeIn);

		Date endTimeIn = inFormat.parse(endTimeString);
		String endTimeOut = outFormat.format(endTimeIn);

	

		Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(startTimeOut);

		Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(endTimeOut);

		when(
				datasetWS.getDataPersistorDAO().getIMSIByDateRange(startTime,
						endTime)).thenReturn(resultList);
		assertEquals(
				datasetWS.getIMSIByDateRange(startTimeString, endTimeString)
				.size(), 0);
	}
	
	@Test
	public void testManufacturers(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getManufacturers()).thenReturn(resultList);
		assertEquals(
				datasetWS.getManufacturers()
						.size(), 0);

	}
	
	@Test
	public void testGetImsiFailureCount() throws ParseException{
		List<BaseData> resultList =  new ArrayList<BaseData>();
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String startTimeString = "2013-01-11T17:10";
		String endTimeString = "2013-01-11T17:50";
		Date startTimeIn = inFormat.parse(startTimeString);
		String startTimeOut = outFormat.format(startTimeIn);

		Date endTimeIn = inFormat.parse(endTimeString);
		String endTimeOut = outFormat.format(endTimeIn);


		Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(startTimeOut);

		Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(endTimeOut);
		

		when(
				datasetWS.getDataPersistorDAO().getIMSIFailureCount("191911000002886",startTime,
						endTime)).thenReturn(resultList);
		assertEquals(
				datasetWS. getIMSIFailureCount("191911000002886",startTimeString, endTimeString)
				.size(), 0);
		
	}
	
	@Test
	public void testGetFailureCountByModel() throws ParseException{
		List<BaseData> resultList =  new ArrayList<BaseData>();
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String startTimeString = "2013-01-11T17:10";
		String endTimeString = "2013-01-11T17:50";
		Date startTimeIn = inFormat.parse(startTimeString);
		String startTimeOut = outFormat.format(startTimeIn);

		Date endTimeIn = inFormat.parse(endTimeString);
		String endTimeOut = outFormat.format(endTimeIn);


		Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(startTimeOut);

		Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(endTimeOut);
		

		when(
				datasetWS.getDataPersistorDAO().getFailureCountByModel("G410",startTime,
						endTime)).thenReturn(resultList);
		assertEquals(
				datasetWS.getFailureCountByModel("G410",startTimeString,
						endTimeString)
				.size(), 0);
		
	}
	
	@Test
	public void testGetImsiForAutoComplete(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getIMSIFailures("191")).thenReturn(resultList);
		assertEquals(
				datasetWS.getAllIMSIs("191")
						.size(), 0);

	}
	
	@Test
	public void testGetIMSIFailureCauseCodes(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getIMSIFailures("191911000002886")).thenReturn(resultList);
		assertEquals(
				datasetWS.getIMSIFailures("191911000002886")
						.size(), 0);
	}
	
	@Test
	public void testGetIMSIFailureClass(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getIMSIFailureClass("191911000002886",4)).thenReturn(resultList);
		assertEquals(
				datasetWS.getIMSIFailureClass("191911000002886",4)
						.size(), 0);
	}
	
	@Test
	public void testMarketingName(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getMarketingName("Mitsubishi")).thenReturn(resultList);
		assertEquals(
				datasetWS.getMarketingName("Mitsubishi")
						.size(), 0);
	}
	
	@Test
	public void testGetImsiWithFailures(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getIMSIWithFailures()).thenReturn(resultList);
		assertEquals(
				datasetWS.getIMSIWithFailure().size(), 0);
	}
	
	@Test
	public void testGetUniqueEventId(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getUniqueEventId("G410","Mitsubishi")).thenReturn(resultList);
		assertEquals(
				datasetWS.getUniqueEventId("G410","Mitsubishi").size(), 0);
	}
	
	@Test
	public void testGetUniqueEventIdCauseCode(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getUniqueEventIdCauseCode("G410","Mitsubishi")).thenReturn(resultList);
		assertEquals(
				datasetWS.getUniqueEventIdCauseCode("G410","Mitsubishi").size(), 0);
	}
	
	@Test
	public void testGetCountUniqueEventIdCauseCode(){
		List<BaseData> resultList =  new ArrayList<BaseData>();
		when(
				datasetWS.getDataPersistorDAO().getCountUniqueEventIdCauseCode("G410","Mitsubishi")).thenReturn(resultList);
		assertEquals(
				datasetWS.getCountUniqueEventIdCauseCode("G410","Mitsubishi").size(), 0);
	}
	
	
	
	

	@Test
	public void testFileUploadSuccess() throws Exception {
		/*
		 * List<String> resultList = new ArrayList<String>(); FileInputStream
		 * fin = mock(FileInputStream.class); byte[] bytes = new byte[10];
		 * 
		 * when(fin.read(bytes)).thenReturn(1).thenReturn(2);
		 * assertEquals(datasetWS.uploadFile(fin).size(), 0);
		 */
	}

	@Test(expected = IOException.class)
	public void testFileUploadFail() throws Exception {
		// TODO
		throw new IOException();
	}

	@Test
	public void testPersistDataTableSuccess() throws Exception {
		// TODO
	}

	@Test
	public void testPersistDataTableFail() throws Exception {
		// TODO
	}
}
