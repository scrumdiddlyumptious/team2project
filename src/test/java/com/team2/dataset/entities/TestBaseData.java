package com.team2.dataset.entities;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;



public class TestBaseData {
	private BaseData baseData;

	@Before
	public void setUp() throws Exception {
		baseData = new BaseData();
		assertNotNull(baseData);
		baseData.toString();
	}

	@Test
	public void testGetAndSetId() {
		baseData.setId(100);
		assertEquals(100, baseData.getId());
	}
	
	@Test
	public void testGetAndSetDate() {
		Date date = new Date();
		baseData.setDate(date);
		assertEquals(date, baseData.getDate());
	}
	
	@Test
	public void testGetAndSetEventId() {
		baseData.setEventId(4098);
		assertEquals(4098, baseData.getEventId());
	}
	@Test
	public void testGetAndSetFailureClass() {
		baseData.setFailureClass(0);
		assertEquals(0, baseData.getFailureClass());
	}
	
	@Test
	public void testGetAndSetUserEquipment() {
		baseData.setUserEquipment(33001735);
		assertEquals(33001735, baseData.getUserEquipment());
	}
	
	@Test
	public void testGetAndSetMarket() {
		baseData.setMarket(405);
		assertEquals(405, baseData.getMarket());
	}
	
	@Test
	public void testGetAndSetOperator() {
		baseData.setOperator(1);
		assertEquals(1, baseData.getOperator());
	}
	
	@Test
	public void testGetAndSetCellId() {
		baseData.setCellId(4);
		assertEquals(4, baseData.getCellId());
	}
	
	@Test
	public void testGetAndSetDuration() {
		baseData.setDuration(1005);
		assertEquals(1005, baseData.getDuration());
	}
	
	@Test
	public void testGetAndSetCauseCode() {
		baseData.setCauseCode(1);
		assertEquals(1, baseData.getCauseCode());
	}
	
	@Test
	public void testGetAndSetNeVersion() {
		baseData.setNeVersion("13A");
		assertEquals("13A", baseData.getNeVersion());
	}
	
	@Test
	public void testGetAndSetImsi() {
		baseData.setImsi("1919110456426");
		assertEquals("1919110456426", baseData.getImsi());
	}
	
	@Test
	public void testGetAndSetHier3_ID() {
		baseData.setHier3_ID(new BigInteger("5101480358281000000"));
		assertEquals(new BigInteger("5101480358281000000"), baseData.getHier3_ID());
	}
	
	@Test
	public void testGetAndSetHier32_ID() {
		baseData.setHier32_ID(new BigInteger("4462714857032800000"));
		assertEquals(new BigInteger("4462714857032800000"), baseData.getHier32_ID());
	}
	
	@Test
	public void testGetAndSetHier321_ID() {
		baseData.setHier321_ID(new BigInteger("1650127677358040000"));
		assertEquals(new BigInteger("1650127677358040000"), baseData.getHier321_ID());
	}

}
