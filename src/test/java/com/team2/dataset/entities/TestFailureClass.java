package com.team2.dataset.entities;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestFailureClass {
	private FailureClass failureClass;

	@Before
	public void setUp() throws Exception {
		
		failureClass = new FailureClass();
		assertNotNull(failureClass);
		failureClass.toString();
	}

	@Test
	public void testGetSetFailureCassId() {
		failureClass.setFailureClassID(1);
		assertEquals(1,failureClass.getFailureClassID());
		
	}
	
	@Test
	public void testGetSetDescription() {
		failureClass.setDescription("HIGH PRIORITY ACCESS");
		assertEquals("HIGH PRIORITY ACCESS",failureClass.getDescription());
	}

}
