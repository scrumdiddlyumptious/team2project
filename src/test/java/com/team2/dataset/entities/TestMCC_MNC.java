package com.team2.dataset.entities;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestMCC_MNC {
	private MCC_MNC mccMnc;

	@Before
	public void setUp() throws Exception {
		 mccMnc= new MCC_MNC();
		 assertNotNull(mccMnc);
		 mccMnc.toString();
	}

	@Test
	public void testGetAndSetMcc() {
		mccMnc.setMobileCountryCode(238);	
		assertEquals(238,mccMnc.getMobileCountryCode());
	}
	
	@Test
	public void testGetAndSetMnc() {
		mccMnc.setMobileNetworkCode(1);	
		assertEquals(1,mccMnc.getMobileNetworkCode());
	}
	
	@Test
	public void testGetAndSetCountry() {
		mccMnc.setCountry("Denmark");	
		assertEquals("Denmark",mccMnc.getCountry());
	}
	
	@Test
	public void testGetAndSetOperator() {
		mccMnc.setOperator("TDC-DK");	
		assertEquals("TDC-DK",mccMnc.getOperator());
	}


}
