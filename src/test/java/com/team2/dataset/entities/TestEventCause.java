package com.team2.dataset.entities;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestEventCause {
	private EventCause eventCause;

	@Before
	public void setUp() throws Exception {
		eventCause = new EventCause();
		assertNotNull(eventCause);
		eventCause.toString();
	}

	@Test
	public void testGetAndSetCauseCode() {
		eventCause.setCauseCode(0);
		assertEquals(0, eventCause.getCauseCode());
	}
	
	@Test
	public void testGetAndSetEventId() {
		eventCause.setEventId(4097);
		assertEquals(4097, eventCause.getEventId());

	}
	
	@Test
	public void testGetAndSetDescription() {
		eventCause.setDescription("RRC CONN SETUP-SUCCESS");
		assertEquals("RRC CONN SETUP-SUCCESS", eventCause.getDescription());

	}

}
