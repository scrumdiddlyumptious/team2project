package com.team2.dataset.entities;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestUserEquipment {
	private UserEquipment ue;

	@Before
	public void setUp() throws Exception {
		 ue= new UserEquipment();
		 assertNotNull(ue);
		 ue.toString();
	}

	@Test
	public void testSetAndGetTac() {
		ue.setTypeAllocationCode(100100);
		assertEquals(100100,ue.getTypeAllocationCode());
	}
	
	@Test
	public void testSetAndGetMarketingName() {
		ue.setMarketingName("G410");
		assertEquals("G410",ue.getMarketingName());
	}
	
	@Test
	public void testSetAndGetManufacturer() {
		ue.setManufacturer("Mitsubishi");
		assertEquals("Mitsubishi",ue.getManufacturer());
	}
	
	@Test
	public void testSetAndGetAcessCapability() {
		ue.setAccessCapability("GSM 1800, GSM 900");
		assertEquals("GSM 1800, GSM 900",ue.getAccessCapability());
	}
	
	@Test
	public void testSetAndGetModel() {
		ue.setModel("G410");
		assertEquals("G410",ue.getModel());
	}
	
	@Test
	public void testSetAndGetVendorName() {
		ue.setVendorName("Mitsubishi");
		assertEquals("Mitsubishi",ue.getVendorName());
	}
	
	@Test
	public void testSetAndGetUeType() {
		ue.setUeType("(null)");
		assertEquals("(null)",ue.getUeType());
		ue.toString();
	}
	
	@Test
	public void testSetAndGetOs() {
		ue.setOs("(null)");
		assertEquals("(null)",ue.getOs());
	}
	
	@Test
	public void testSetAndGetInputMode() {
		ue.setInputMode("(null)");
		assertEquals("(null)",ue.getInputMode());
	}

}
