package com.team2.user.businesslogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.team2.user.businesslogic.UserDAO;
import com.team2.user.businesslogic.UserWS;
import com.team2.user.entities.User;
import com.team2.user.entities.UserType;

public class TestUserWS {

	private UserWS userWS = new UserWS();
	private User user;
	private String id = "user";
	private String password = "password";

	@Before
	public void injectMockUserDAO() throws Exception {
		UserDAO userDAO = mock(UserDAO.class);
		userWS.setUserDAO(userDAO);
	}
	
	@Before
	public void createUser() throws Exception {
		user = new User();
		user.setId(id);
		user.setPassword(password);
	}

	@Test
	public void testGetUserById() {
		when(userWS.getUserDAO().getUser("user")).thenReturn(user);
		assertEquals(userWS.getUser("user"), user);
	}

	@Test
	public void testGetUserByIdNotFound() {
		when(userWS.getUserDAO().getUser("user")).thenReturn(null);
		assertNull(userWS.getUser("user"));
	}

	@Test
	public void testAddUserSuccess() {
		String successMessage = "{\"message\":\"User " + user.getId()
				+ " successfully added to the database.\"}";
		assertEquals(successMessage, userWS.addUser(user));

		ArgumentCaptor<User> addedUser = ArgumentCaptor.forClass(User.class);
		verify(userWS.getUserDAO(), atLeast(0)).addUser(addedUser.capture());
	}

	@Test
	public void testAddUserFail() {
		when(userWS.getUserDAO().getUser(user.getId())).thenReturn(user);
		
		String failureMessage = "{\"message\":\"User " + user.getId()
				+ " already exists in the database.\"}";
		assertEquals(failureMessage, userWS.addUser(user));
	}
	
	@Test
	public void testAdminLoginOk() {
		user.setUserType(UserType.ADMINISTRATOR.getType());

		when(userWS.getUserDAO().getUser(id)).thenReturn(user);
		assertEquals("Administrator", userWS.userLogin(id, password));
	}
	
	@Test
	public void testCustServiceRepLoginOk() {
		user.setUserType(UserType.CUSTOMER_SERVICE_REP.getType());
		
		when(userWS.getUserDAO().getUser(id)).thenReturn(user);
		assertEquals("Customer Service Rep.", userWS.userLogin(id, password));
	}
	
	@Test
	public void testSupportEngLoginOk() {
		user.setUserType(UserType.SUPPORT_ENGINEER.getType());
		
		when(userWS.getUserDAO().getUser(id)).thenReturn(user);
		assertEquals("Support Engineer", userWS.userLogin(id, password));
	}
	
	@Test
	public void testNetworkMgmtEngLoginOk() {
		user.setUserType(UserType.NETWORK_MANAGEMENT_ENGINEER.getType());
		
		when(userWS.getUserDAO().getUser(id)).thenReturn(user);
		assertEquals("Network Management Engineer", userWS.userLogin(id, password));
	}

	@Test
	public void testUserLoginWrongPassword() {
		String incorrectPassword = "test";

		when(userWS.getUserDAO().getUser(id)).thenReturn(user);
		assertEquals("FAIL", userWS.userLogin(id, incorrectPassword));
	}
	
	@Test
	public void testUserLoginUnknownUser() {
		String incorrectId = "someone";
		String incorrectPassword = "test";

		when(userWS.getUserDAO().getUser(incorrectId)).thenReturn(null);
		assertEquals("FAIL", userWS.userLogin(incorrectId, incorrectPassword));
	}

}
