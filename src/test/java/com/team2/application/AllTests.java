package com.team2.application;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.team2.dataset.businesslogic.TestDatasetDAO;
import com.team2.dataset.businesslogic.TestDatasetWS;
import com.team2.dataset.entities.TestBaseData;
import com.team2.dataset.entities.TestEventCause;
import com.team2.dataset.entities.TestFailureClass;
import com.team2.dataset.entities.TestMCC_MNC;
import com.team2.dataset.entities.TestUserEquipment;
import com.team2.dataset.validator.TestBaseDataValidator;
import com.team2.dataset.validator.TestDataValidator;
import com.team2.dataset.validator.TestEventObjectValidator;
import com.team2.dataset.validator.TestFailureClassValidator;
import com.team2.dataset.validator.TestMCC_MNCValidator;
import com.team2.dataset.validator.TestUserEquipmentValidator;
import com.team2.user.businesslogic.TestUserDAO;
import com.team2.user.businesslogic.TestUserWS;
import com.team2.user.entities.TestUser;
import com.team2.dataset.businesslogic.TestEntityObjectFactory;

@RunWith(Suite.class)
@SuiteClasses({ TestUser.class, TestUserDAO.class, TestUserWS.class,
	TestDatasetWS.class, TestDatasetDAO.class, TestFailureClassValidator.class, TestEventObjectValidator.class,
	TestUserEquipmentValidator.class, TestMCC_MNCValidator.class, TestBaseDataValidator.class,TestEntityObjectFactory.class,
	TestBaseData.class, TestEventCause.class, TestFailureClass.class, TestUserEquipment.class, TestMCC_MNC.class, TestDataValidator.class })
public class AllTests {

}
