/**
 * 
 */

var topTenIMSIFailuresInit = function() {
	$('#submitButtonNWE04').click(
			function() {
				var startTime = $('#startTimeNWE04').val();
				var endTime = $('#endTimeNWE04').val();

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/TopTenIMSIFailures/"
							+ startTime + "&" + endTime,
					dataType : "json",
					async : false,
					success : renderList
				});
				$("#topTenIMSIFailuresTable").show();
			});

	$('#resetButtonNWE04').click(function() {
		$('#startTimeNWE04').val('');
		$('#endTimeNWE04').val('');
		$('#topTenIMSIFailuresTable').dataTable().fnDestroy();
		$('#tableBodyNWE04').empty();
	});

};

var renderList = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#tableBodyNWE04').empty();

	$.each(list, function(index, data) {

		$('#tableBodyNWE04').append(
				"<tr><td>" + data[0] + "</td><td>" + data[1] + "</td></tr>");
	});
	$('#topTenIMSIFailuresTable').dataTable({
		"order" : [ [ 1, "desc" ] ]
	});

};