//
$(function() {
	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 460,
		width: 410,
		modal: true,
		buttons: {
			"Log in": function() {
				var userId = $("#userId").val();
				var password = $("#password2").val();
				console.log("login submit: " +userId +", "+ password);
				var accessPermission = authenticate(userId, password);
				displayTabs(userId,accessPermission);
				$( this ).dialog( "close" );
				return false;
			}
		},
		Cancel: function() {
			$( this ).dialog( "close" );
		},
		close: function() {
			// allFields.val( "" ).removeClass( "ui-state-error" );

			console.log("closing login dialog box");
		}
	});

	$("#login").button().click(function() {
		console.log("Login button pressed");
		$( "#dialog-form" ).dialog( "open" );
	});
	//logout dialog
	$( "#dialog-logout" ).dialog({
		autoOpen: false,
		height: 200,
		width: 300,
		modal: true,
		open: function(){
			$("#dialog-logout").html("Are you sure you want to log out?");
		},
		buttons:{
			"Ok":function() {
				//$( this ).dialog("close");
				$("#dialog-logout").html("Logging out...");
				loggingOut();
			},
			"Close": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			// allFields.val( "" ).removeClass( "ui-state-error" );

			console.log("closing logout dialog box");
		}
	});

	$("#logout").button().click(function() {
		console.log("Logout button pressed");
		$( "#dialog-logout" ).dialog( "open" );
		//window.clearTimeout(loggingOut());//so that it only happens once
	});
});

var displayTabs = function (userId, accessPermission){
	console.log("displayTabs(): " + userId + "," + accessPermission);
	switch(accessPermission){
	case "Administrator": 
		hideTabs();
		console.log("case: " + accessPermission);
		$(".tab-admin01").show();
		$(".tab-admin02").show();
		$(".tab-admin03").show();
		initCreateEmployeeForm();
		initUploadForm();
		loadBaseData();
		loginSuccess(userId, accessPermission);
		break;
	case "Customer Service Rep": 
		hideTabs();
		console.log("case: " + accessPermission);
		showCSRTabs();
		loginSuccess(userId, accessPermission);
		break;
	case "Support Engineer": 
		hideTabs();
		console.log("case: " + accessPermission);
		$(".tab-SE01").show();
		$(".tab-SE02").show();
		$(".tab-SE03").show();
		showCSRTabs();
		$(".imsiCallFailuresTableSE01").hide();
		$(".imsiCallFailuresTableSE02").hide();
		imsiqueryinit();
		initManufacturersQuery();
		failureClassSearchIMSI();
		loginSuccess(userId, accessPermission);
		break;
	case "Network Management Engineer": 
		hideTabs();
		console.log("case: " + accessPermission);
		$(".tab-NWE01").show();
		$(".tab-NWE02").show();
		$(".tab-NWE03").show();
		$(".tab-SE01").show();
		$(".tab-SE02").show();
		$(".tab-SE03").show();
		showCSRTabs();
		imsiCallFailuresQueryInit();
		initNetworkManagementEngineerQuery();
		top10comboinit();
		topTenIMSIFailuresInit();
		loginSuccess(userId, accessPermission);
		break;
	case "FAIL":
		console.log("case: " + accessPermission);
		$('#error').append("User Login Failed: Wrong Username or Password");
		return false;
		break;
	default :
		console.log("default:" +accessPermission);
	$('#error').append("Oops! Something went wrong there.");
	return false;
	break;
	}
};

var showCSRTabs = function(){
	console.log("showCSRTabs() fired");
	$(".tab-CSR01").show();
	$(".tab-CSR02").show();
	imsiSearch();
	imsiqueryinitCSR();
	$(".imsiCSRtable").hide();
};

var loggingOut = function(){
	setTimeout(function(){
		$("#dialog-logout").html("You have successfully logged out.");
		sessionStorage.clear();
		document.getElementById("userMessage").innerHTML = "You need to log in. ";
		hideTabs();
		location.reload();
		$(".logout").hide();
		$(".login").show();
	}, 1000);

};

var loginSuccess=function(userId, accessPermission){
	console.log("login success");
	sessionStorage.userId=userId;
	sessionStorage.accessLevel=accessPermission;
	document.getElementById("userMessage").innerHTML = "User Id: " + sessionStorage.userId +
	",\tAccess type : " + sessionStorage.accessLevel;
	$(".login").hide();
	$(".logout").show();
	$("#userId").val("");
	$("#password2").val("");
};
var hideTabs = function(){
	//if you create a tab hide it here
	$(".tab-admin01").hide();
	$(".tab-admin02").hide();
	$(".tab-admin03").hide();
	$(".tab-NWE01").hide();
	$(".tab-NWE02").hide();
	$(".tab-NWE03").hide();
	$(".tab-CSR01").hide();
	$(".tab-CSR02").hide();
	$(".tab-SE01").hide();
	$(".tab-SE02").hide();
	$(".tab-SE03").hide();
};

var authenticate = function(userId, password) { 
	console.log("authenticate userId: " + userId);
	var responseText = '';

	$.ajax({ 
		type: 'GET', 
		url: "/Team2Project/rest/users/login/" + userId + "&" + password, 
		dataType: 'text', 
		contentType: 'text/plain', 
		async: false, 
		success: function(text) { 
			responseText = text; 
		}, 
		error: function(text) { 
			responseText = text; 
		} 
	}); 
	console.log("authenticate() " + responseText);
	return responseText; 
};

$(document).ready(function(){
	$( "#tabs" ).tabs();
	hideTabs();
	$(".logout").hide();
	if(sessionStorage.accessLevel=="Administrator"||
			sessionStorage.accessLevel=="Customer Service Rep"||
			sessionStorage.accessLevel=="Support Engineer"||
			sessionStorage.accessLevel=="Network Management Engineer"){
		console.log(sessionStorage.userId + ", "+ sessionStorage.accessLevel);
		displayTabs(sessionStorage.userId,sessionStorage.accessLevel);
		$(".logout").show();
	}else{
		console.log("Session not set");
	}
});
