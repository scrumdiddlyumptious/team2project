var initNetworkManagementEngineerQuery = function() {
	
	accordionReset();

	$("#errorMessage").dialog({
		autoOpen : false,
		width : 400,
		buttons : [ {
			text : "Close",
			click : function() {
				$(this).dialog("close");
			}
		} ]

	});
	

	$('#eventIdQueryTab').click(
			function() {

				var selectedManufacturer = $('#manufacturerName').val();
				var modelName = $('#modelName').val();

				if (modelName == null | selectedManufacturer == null) {

					var message = 'Please select a Manufacturer and Model';
					displayErrorMessage(message);
					$('#errorMessage').dialog('open');

				} else {

					var manufacturerName = selectedManufacturer.replace("&", "%26");

					$.ajax({
						type : 'GET',
						url : "/Team2Project/rest/dataset/getUniqueEventId/"
								+ modelName + "&" + manufacturerName,
						dataType : "json",
						async : false,
						success : renderUniqueEventId
					});
				}

			});
	
	$('#eventIdCauseCodeTab').click(function() {
				var selectedManufacturer = $('#manufacturerName').val();
				var modelName = $('#modelName').val();

				if (modelName == null | selectedManufacturer == null) {

					var message = 'Please select Manufacturer and Model';
					displayErrorMessage(message);

				} else {

					var manufacturerName = selectedManufacturer.replace("&", "%26");

					$.ajax({
							type : 'GET',
							url : "/Team2Project/rest/dataset/getUniqueEventIdCauseCode/"
									+ modelName
									+ "&"
									+ manufacturerName,
							dataType : "json",
							async : false,
							success : renderUniqueEventIdCauseCode
							});
				}

			});
	
	$('#uniqueComboCountTab').click(function() {

				var selectedManufacturer = $('#manufacturerName').val();
				var modelName = $('#modelName').val();

				if (modelName == null | selectedManufacturer == null) {

					var message = 'Please select a Manufacturer and Model';
					displayErrorMessage(message);

				} else {

					var manufacturerName = selectedManufacturer.replace("&", "%26");

					$.ajax({
							type : 'GET',
							url : "/Team2Project/rest/dataset/getCountUniqueEventIdCauseCode/"
									+ modelName
									+ "&"
									+ manufacturerName,
							dataType : "json",
							async : false,
							success : renderCountUniqueEventIdCauseCode
							});
				}
	});

	// ---------------------------------- Ajax request for phone model Query
	

	$('#manufacturerName').change(
			function() {
				accordionReset();
				var selectedManfacturer = $('#manufacturerName').val();
				var manufacturerName = selectedManfacturer.replace("&", "%26");

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/getMarketingName/"
							+ manufacturerName,
					dataType : "json",
					async : false,
					success : renderModelList
				});
	});
	
	
	$('#modelName').change(
			function() {
				accordionReset();
		});
	
	// ---------------------- Ajax request for manufacturer names
	
	$.ajax({
		type : 'GET',
		url : "/Team2Project/rest/dataset/getManufacturers/",
		dataType : "json",
		async : false,
		success : renderManufacturerList
	});

};


//---------------------------- accordion

var accordionReset = function() {
	$( "#accordion" ).accordion({ 
	
		active: false,
		collapsible: true,	
		heightStyle: "content",
		autoHeight: false,
		clearStyle: true,  
		
	});
	
	clearTables();
};

// ---------------------- render dropdown for manufacturer model


var renderManufacturerList = function(data) {

	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);

	$.each(list, function(index, data) {

		$('#manufacturerName').append(
				"<option value='" + data + "'>" + data + "</option>");
	});
};

// ----------------------- render dropdown for model list


var renderModelList = function(data) {

	$('#modelName').empty();
	$('#modelName').append("<option selected disabled>Select Model</option>");

	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);

	$.each(list, function(index, data) {

		$('#modelName').append(
				"<option value='" + data + "'>" + data + "</option>");

	});

};


// ------------------------ render Results of getUniqueEventId Query


var renderUniqueEventId = function(data) {

	$("#eventIdTableBody").empty();

	var manufacturerName = $('#manufacturerName').val();
	var modelName = $('#modelName').val();

	if (data.length == 0) {

		var message = 'No Data available for this Model';
		displayErrorMessage(message);

	} else {

		var list = data == null ? []
				: (data instanceof Array ? data : [ data ]);

		$.each(list, function(index, data) {

			$('#eventIdTableBody').append(
					"<tr><th>" + data[1] + "</th><td>" + manufacturerName
							+ "</td><td>" + modelName + "</td><td>" + data[0]
							+ "</td></tr>");
		});
	}
	$('#uniqueEventIdTable').dataTable();
};

// ------------------------ render Results of getUniqueEventIdCauseCode Query


var renderUniqueEventIdCauseCode = function(data) {

	$('#eventIdCauseCodeTableBody').empty();

	if (data.length == 0) {

		var message = 'No Data available for this Model';
		displayErrorMessage(message);
		

	} else {

		var manufacturerName = $('#manufacturerName').val();
		var modelName = $('#modelName').val();
		
		var list = data == null ? []
				: (data instanceof Array ? data : [ data ]);

		$.each(list, function(index, data) {

			$('#eventIdCauseCodeTableBody').append(
					"<tr><th>" + data[2] + "</th><td>" + manufacturerName
							+ "</td><td>" + modelName + "</td><td>" + data[0]
							+ "</td><td>" + data[1] + "</td></tr>");
		});
	}
	$('#eventIdCauseCodeTable').dataTable();
};



// ----------- render Results of getCountUniqueEventIdCauseCode Query 

var renderCountUniqueEventIdCauseCode = function(data) {

	$('#eventIdCauseCodeCountTableBody').empty();

	if (data.length == 0) {

		var message = 'Please select a Manufacturer and Model';
		displayErrorMessage(message);

	} else {

		var manufacturerName = $('#manufacturerName').val();
		var modelName = $('#modelName').val();

		var list = data == null ? []
				: (data instanceof Array ? data : [ data ]);

		$.each(list, function(index, data) {

			$('#eventIdCauseCodeCountTableBody').append(
					"<tr><th>" + data[3] + "</th><td>" + manufacturerName
							+ "</td><td>" + modelName + "</td><td>" + data[0]
							+ "</td><td>" + data[1] + "</td><td>" + data[2]
							+ "</td></tr>");
		});

	}
	
	$('#uniqueEventIdCauseCodeCountTable').dataTable();
};

// ---------------------------------------- Error Message Dialog box


var displayErrorMessage = function(message) {
	
	accordionReset();
	$('#errorMessage').empty();
	$('#errorMessage').append('<div>' + message + '</div>');
	$('#errorMessage').dialog('open');

};


//-------------------- clear data tables

var clearTables = function() {
	
	$("#eventIdTableBody").empty();
	$('#eventIdCauseCodeTableBody').empty();
	$('#eventIdCauseCodeCountTableBody').empty();
	
};