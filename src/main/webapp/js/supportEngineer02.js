/**
 * 
 */

// $(document).ready(function(){
var initManufacturersQuery = function() {
	// ---------------------- Ajax request for manufacturer names
	// --------------------

	$('#submitButtonSE02').click(
			function() {
				var startTime = $('#startTimeSE02').val();
				var endTime = $('#endTimeSE02').val();
				var selectedM = $('#manufacturerNameSE02').val();
				var manufacturerName = selectedM.replace("&", "%26");
				var modelName = $('#modelNameSE02').val();

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/failurecountbymodel/"
							+ manufacturerName + "&" + modelName + "&"
							+ startTime + "&" + endTime,
					dataType : "json",
					async : false,
					success : renderListSE02
				});

			});

	$('#resetButtonSE02').click(function() {
		$('#startTimeSE02').val('');
		$('#endTimeSE02').val('');
		$('#manufacturerNameSE02').val('');
		$('#modelNameSE02').val('');
		$('#phoneModelTableBody').empty();
	});

	// });

	// ---------------------------------- Ajax request for phone model names
	// -------------------------------

	$('#manufacturerNameSE02').change(
			function() {

				var selectedM = $('#manufacturerNameSE02').val();
				var manufacturerName = selectedM.replace("&", "%26");

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/getMarketingName/"
							+ manufacturerName,
					dataType : "json",
					async : false,
					success : renderModelListSE02
				});
			});

	$.ajax({
		type : 'GET',
		url : "/Team2Project/rest/dataset/getManufacturers/",
		dataType : "json",
		async : false,
		success : renderManufacturerListSE02
	});

};// End of Document.Read

// ---------------------- render dropdown for manufacturer
// ----------------------------------------

var renderManufacturerListSE02 = function(data) {
	console.log("renderManufacturerList");

	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);

	$.each(list, function(index, data) {

		$('#manufacturerNameSE02').append(
				"<option value='" + data + "'>" + data + "</option>");
	});
};

// ----------------------- render dropdown for model
// -------------------------------------

var renderModelListSE02 = function(data) {
	console.log("renderModelList");
	
	$('#modelNameSE02').empty();
	$('#modelNameSE02').append("<option value=''>Select Model</option>");

	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);

	$.each(list, function(index, data) {

		$('#modelNameSE02').append(
				"<option value='" + data + "'>" + data + "</option>");

	});

};

/*
 * $(function() { $('#submitButton').click( function() { var startTime =
 * $('#startTime').val(); var endTime = $('#endTime').val();
 * 
 * var selectedM = $('#manufacturerName').val(); var manufacturerName =
 * selectedM.replace("&", "%26"); var modelName = $('#modelName').val();
 * 
 * $.ajax({ type : 'GET', url :
 * "/Team2Project/rest/dataset/failurecountbymodel/" + manufacturerName + "&" +
 * modelName + "&" + startTime + "&" + endTime, dataType : "json", async :
 * false, success : renderList });
 * 
 * });
 * 
 * $('#resetButton').click(function() { $('#startTime').val('');
 * $('#endTime').val(''); $('#phoneModelTableBody').empty(); }); });
 */

var renderListSE02 = function(data) {
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#phoneModelTableBody').empty();

	$.each(list,
			function(index, data) {
				var startDate = new Date(data[2]);
				var endDate = new Date(data[3]);

				var startDateString = commonDateFormat(startDate);
				var endDateString =commonDateFormat(endDate);

				$('#phoneModelTableBody').append(
						// "<tr><td>" + data + "</td></tr>");
						"<tr><td>" + data[0] + "</td><td>" + data[1]
								+ "</td><td>" + startDateString + "</td><td>"
								+ endDateString + "</td><td>" + data[4]
								+ "</td></tr>");
			});
	$('#phoneModelTable').dataTable();
};
