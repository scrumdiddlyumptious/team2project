
/* Story 4.1 */

var imsiSearch = function() {

	$( '#imsiField' ).autocomplete({
		source: function( request, response ) {  
			var imsi = $('#imsiField').val();
			$.ajax({
				url: "/Team2Project/rest/dataset/imsiAutocomplete/" + imsi ,
				dataType: "json",
				data: {
					q: request.term
				},
				success: function( data ) {
					response( data );
				}
			});
		},
		minLength: 4,  
	});


	//4.2 IMSI + Failure search
	$("#dialog-imsi").dialog({
		autoOpen: false,
		title: "IMSI Failure Search",

		height: 280,
		width: 410,
		modal: true,
		buttons: {
			"Find": function() {
				var imsi = $('#imsiField').val();
				var imsiFailure = $('#imsiFailureSelect').val();

				$.ajax({
					type : 'GET',
					url : "/Team2Project/rest/dataset/getIMSIFailureClass/"+ imsi + "&" + imsiFailure,
					dataType : "json",
					async : false,
					success : IMSIFailureClassData
				});
				$('#dialog-imsi').dialog('close');

			}
		},
		Cancel: function() {
			$( this ).dialog( "close" );
		},

	});



	//4.1 View all IMSIs
	$('#submitIMSIQuery').click(

			function() {
				$("#imsiCSRtable").dataTable().fnDestroy();
				var imsi = $('#imsiField').val();
				if(validateIMSI(imsi)){
					$.ajax({
						type : 'GET',
						url : "/Team2Project/rest/dataset/getIMSIFailures/" + imsi,
						dataType : "json",
						async : false,
						success : IMSIFailureData
					});

				}
			});

	$('#submitIMSIQuery2').click(
			function() {
				clearHideTable();
				if(validateIMSI($('#imsiField').val())){	
					$( "#dialog-imsi" ).dialog( "open" );
				}
			});

	$('#submitIMSIQuery3').click(
			function() {
				$("#imsiCSRtable").dataTable().fnDestroy();
				var table = $('#imsiCSRtable').DataTable();

				table.column( 0 ).visible( false );

				var imsi = $('#imsiField').val();
				if(validateIMSI(imsi)){
					$.ajax({
						type : 'GET',
						url : "/Team2Project/rest/dataset/imsifailurecausecodes/" + imsi,
						dataType : "json",
						async : false,
						success : IMSIFailureData02
					});

				}
			});

	$('#resetIMSIButton').click(function() {
		$('#imsiField').val('');
		clearHideTable();
	});

};

function renderDatatable02(data) {
	
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#imsi_table_body').empty();

		$.each(list, function(index, data) {
			$('#imsi_table_body').append(
					"<tr><td>" + data[0] + "</td><td>" + data[1] + "</td></tr>");
		});

		
		$('#imsiCSRtable').dataTable();
		$("#imsiCSRtable").show();
};
var IMSIFailureData02 = function(data) {
		if(data.length == 0){		
			$("#imsiMessage").html( "<h2>There are no results for that IMSI with that search criteria.</h2>" );
		}else{
		
		var imsi = $('#imsiField').val();
		$("#imsiMessage").html( "<h2>Results for IMSI id: " + imsi +"</h2>" );

		renderDatatable02(data);
		}
	};
var IMSIFailureData = function(data) {
	if(data.length == 0){		
		$("#imsiMessage").html( "<h2>There are no results for that IMSI with that search criteria.</h2>" );
	}else{

		var imsi = $('#imsiField').val();
		$("#imsiMessage").html( "<h2>Results for IMSI id: " + imsi +"</h2>" );
		renderDatatable(data);
	}
};

var IMSIFailureClassData = function(data) {
	if(data.length == 0){		
		$("#imsiMessage").html( "<h2>There are no results for that IMSI with that search criteria.</h2>" );
	}else{

		var imsi = $('#imsiField').val();
		var failureType = $('#imsiFailureSelect option:selected').text();
		$("#imsiMessage").html("<h2>Results for IMSI id: " + imsi
				+ " with failure type '" + failureType+ "' are:</h2>");
		renderDatatable(data);
	}
};

function renderDatatable(data) {

	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	$('#imsi_table_body').empty();

	$.each(list, function(index, data) {
		$('#imsi_table_body').append(
				"<tr><td>" + data[0] + "</td><td>"
				+ data[1] + "</td><td>" + data[2] + "</td></tr>");
	});

	$('#imsiCSRtable').dataTable();
	$("#imsiCSRtable").show();
};

var validateIMSI = function (imsi){

	var validated = false;
	if(isNaN(imsi)){
		$("#imsiMessage").html("<h2>" + imsi +" is not a number.</h2>");
	}else if(imsi.length==0){
		$("#imsiMessage").html("<h2>Enter IMSI number above.</h2>");
	}else if(imsi.length==15){
		validated = true;
		return validated;
	}else{
		$("#imsiMessage").html("<h2>" + imsi +" is invalid.</h2>");
	}
	return validated;
};

function clearHideTable() {
	$('#imsiCSRtable').dataTable().fnDestroy();
	$("#imsiCSRtable").hide();
	$("#imsiMessage").html('');
}


