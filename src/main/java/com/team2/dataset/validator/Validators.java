package com.team2.dataset.validator;

import org.apache.poi.hssf.usermodel.HSSFCell;

public interface Validators {
	
	public boolean validate(HSSFCell[] dataObject);

}
