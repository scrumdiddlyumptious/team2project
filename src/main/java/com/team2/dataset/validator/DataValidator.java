package com.team2.dataset.validator;

import org.apache.poi.hssf.usermodel.HSSFCell;

import com.team2.dataset.businesslogic.TableType;

public class DataValidator {

	private static BaseDataValidator baseDataValidator = new BaseDataValidator();
	private static EventObjectValidator eventObjectValidator = new EventObjectValidator();
	private static FailureClassValidator failureClassValidator = new FailureClassValidator();
	private static MCC_MNCValidator mCC_MNCValidator = new MCC_MNCValidator();
	private static UserEquipmentValidator userEquipmentValidator = new UserEquipmentValidator();

	public static boolean validateData(HSSFCell[] dataObject, String sheetName) {
		boolean dataValid = false;
		if (sheetName.equals(TableType.BASE_DATA.getTableName())) {	
			dataValid = baseDataValidator.validate(dataObject);

		} else if (sheetName.equals(TableType.EVENT_CAUSE.getTableName())) {
			dataValid = eventObjectValidator.validate(dataObject);

		} else if (sheetName.equals(TableType.FAILURE_CLASS.getTableName())) {
			dataValid = failureClassValidator.validate(dataObject);

		} else if (sheetName.equals(TableType.MCC_MNC.getTableName())) {
			dataValid = mCC_MNCValidator.validate(dataObject);

		} else if (sheetName.equals(TableType.UE_TABLE.getTableName())) {
			dataValid = userEquipmentValidator.validate(dataObject);
		}

		return dataValid;
	}
}
