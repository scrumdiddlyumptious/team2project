package com.team2.dataset.validator;

import org.apache.poi.hssf.usermodel.HSSFCell;

public class FailureClassValidator implements Validators {

	public boolean validate(HSSFCell[] dataObject) {
		boolean dataValid = true;
		for (HSSFCell dataCell : dataObject) {
			if (dataCell.toString().equals("(null)")) {
				return false;
			}

		}
		if (isInvalidFailureClass(dataObject[0])) {
			dataValid = false;
		}


		
		if(isInvalidDescription(dataObject[1])){
			dataValid=false;
		}
		return dataValid;

	}

	

	private boolean isInvalidFailureClass(HSSFCell failureClassCell) {
		int failureClass = (int) failureClassCell.getNumericCellValue();
		return (failureClass < 0 || failureClass > 4);
	}
	
	private boolean isInvalidDescription(HSSFCell descriptionCell) {
		// TODO Auto-generated method stub
		return descriptionCell.getCellType()!=1;
	}

}
