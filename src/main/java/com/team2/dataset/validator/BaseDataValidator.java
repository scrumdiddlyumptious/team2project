package com.team2.dataset.validator;

import java.math.BigInteger;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;

import com.team2.cache.Cache;
import com.team2.dataset.entities.EventCause;
import com.team2.dataset.entities.FailureClass;
import com.team2.dataset.entities.MCC_MNC;
import com.team2.dataset.entities.UserEquipment;

public class BaseDataValidator implements Validators {
	

	public boolean validate(HSSFCell[] dataObject) {
		

		for (HSSFCell dataCell : dataObject) {
			if (dataCell ==null||dataCell.toString().equals("(null)") ) {
				return false;
			}

		}
		if (isInvalidDateCell(dataObject[0])) {
			return false;
		}

		if (isInValidEventCauseData(dataObject[1], dataObject[8])) {
			return false;
		}

		if (isInvalidFailureClass(dataObject[2])) {
			return false;
		}

		if (isInvalidUEType(dataObject[3])) {
			return false;
		}

		if (isInvalidMccMncData(dataObject[4], dataObject[5])) {
			return false;
		}
		
		if(isInvalidCellId(dataObject[6])){
			return false;
		}


		if (isInvalidDuration(dataObject[7])) {
			return false;
		}

		if (isInvalidNEVersion(dataObject[9])) {
			return false;
		}

		if (isInvalidIMSI(dataObject[10])) {
			return false;
		}
		if (isInvalidHIERID(dataObject[11])) {
			return false;
		}
		
		if (isInvalidHIERID(dataObject[12])) {
			return false;
		}
		if (isInvalidHIERID(dataObject[13])) {
			return false;
		}
		
		

		return true;
	}

	private boolean isInvalidCellId(HSSFCell cellIDCell) {
		int cellId = (int)	cellIDCell.getNumericCellValue();
		return cellId<0||cellId>1000;
	}



	private boolean isInvalidDateCell(HSSFCell dateCell) {

		return !HSSFDateUtil.isCellDateFormatted(dateCell);
	}

	private boolean isInValidEventCauseData(HSSFCell eventIDCell,
			HSSFCell causeCodeCell) {
		
		List<EventCause> eventCauseList = Cache.getEventCauseList();
		int eventID = (int) eventIDCell.getNumericCellValue();
		int causeCode = (int) causeCodeCell.getNumericCellValue();

		for (EventCause eventCause : eventCauseList) {
			if (eventID == eventCause.getEventId()
					&& causeCode == eventCause.getCauseCode()) {
				return false;
			}
		}
		return true;
	
	}

	private boolean isInvalidFailureClass(HSSFCell failureClassCell) {
		List<FailureClass> failureClassList = Cache.getFailureClassList();
		int failureClassID = (int) failureClassCell.getNumericCellValue();

		for (FailureClass fc : failureClassList) {
			if (failureClassID == fc.getFailureClassID()) {
				return false;
			}
		}
		return true;
	}

	private boolean isInvalidUEType(HSSFCell ueTypeCell) {
		List<UserEquipment> userEquipmentList = Cache.getUserEquipmentList();
		int ueType = (int) ueTypeCell.getNumericCellValue();
		for (UserEquipment ue : userEquipmentList) {
			if (ueType == ue.getTypeAllocationCode()) {
				return false;
			}
		}
		return true;

	}

	private boolean isInvalidMccMncData(HSSFCell mccCell, HSSFCell mncCell) {
		List<MCC_MNC> mccMncList = Cache.getMCCMNCList();
		int mcc = (int) mccCell.getNumericCellValue();
		int mnc = (int) mncCell.getNumericCellValue();

		for (MCC_MNC mccMnc : mccMncList) {
			if (mcc == mccMnc.getMobileCountryCode()
					&& (mnc == mccMnc.getMobileNetworkCode())) {
				return false;
			}
		}
		return true;
	}

	private boolean isInvalidDuration(HSSFCell durationCell) {
		int duration = (int) durationCell.getNumericCellValue();
		return duration < 0;
	}

	private boolean isInvalidNEVersion(HSSFCell neVersionCell) {
		String neVersion = neVersionCell.getStringCellValue();
		return !(neVersion.length() == 3);
	}

	private boolean isInvalidIMSI(HSSFCell imsiCell) {
		imsiCell.setCellType(Cell.CELL_TYPE_STRING);
		BigInteger imsi = new BigInteger(imsiCell.getStringCellValue());
		return imsi.compareTo(new BigInteger("100000000000000")) < 0
				|| imsi.compareTo(new BigInteger("400000000000000")) > 0;
		
	}
	
	 private boolean isInvalidHIERID(HSSFCell hierIDCell) {
		 hierIDCell.setCellType(Cell.CELL_TYPE_STRING);
		 BigInteger hierID = new
		 BigInteger(hierIDCell.getStringCellValue());
		 return hierID.compareTo(new
		 BigInteger("1000000000000000"))<0||hierID.compareTo(new
		 BigInteger("999999999999999999999"))>0;
		 }

}
