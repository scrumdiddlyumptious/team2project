package com.team2.dataset.validator;

import org.apache.poi.hssf.usermodel.HSSFCell;

public class EventObjectValidator implements Validators {

	public boolean validate(HSSFCell[] dataObject) {

		boolean dataValid = true;
		for (HSSFCell dataCell : dataObject) {
			if (dataCell.toString().equals("(null)")) {
				return false;
			}

		}
		if (isInvalidCauseCode(dataObject[0])) {
			dataValid = false;
		}

		if (isValidEventId(dataObject[1])) {
			dataValid = false;
		}

		if (isInvalidDescription(dataObject[2])) {
			dataValid = false;
		}

		return dataValid;
	}

	private boolean isInvalidCauseCode(HSSFCell causeCodeCell) {
		int causeCode = (int) causeCodeCell.getNumericCellValue();
		return (causeCode < 0 || causeCode > 33);
	}

	private boolean isValidEventId(HSSFCell eventIdCell) {
		int eventId = (int) eventIdCell.getNumericCellValue();
		return (eventId < 4000 || eventId > 5000);
	}

	private boolean isInvalidDescription(HSSFCell descriptionCell) {
		return descriptionCell.getCellType() != 1;

	}

}
