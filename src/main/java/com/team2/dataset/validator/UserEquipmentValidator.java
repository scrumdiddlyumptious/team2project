package com.team2.dataset.validator;

import org.apache.poi.hssf.usermodel.HSSFCell;

public class UserEquipmentValidator implements Validators {

	public boolean validate(HSSFCell[] dataObject) {
		boolean dataValid = true;
		
		if (isInvalidTac(dataObject[0])) {
			dataValid = false;
		}

		if (isInvalidMarketingName(dataObject[1])) {
			dataValid = false;
		}
		
		if(isInvalidManufacturer(dataObject[2])){
			dataValid=false;
		}

		if (isInvalidAccessCapability(dataObject[3])) {
			dataValid = false;
		}

		if (isInvalidModel(dataObject[4])) {
			dataValid = false;
		}

		if (isInvalidVendorName(dataObject[5])) {
			dataValid = false;
		}

		if (isInvalidUeType(dataObject[6])) {
			dataValid = false;
		}
		if(isInvalidOs(dataObject[7])){
			dataValid = false;
		}

		if (isInvalidInputMode(dataObject[8])) {
			dataValid = false;
		}
		
		return dataValid;

	}

	private boolean isInvalidTac(HSSFCell tacCell) {
		// TODO Auto-generated method stub
		int tac = (int) tacCell.getNumericCellValue();

		return (tac<100000||tac>40000000);
	}

	private boolean isInvalidMarketingName(HSSFCell marketingNameCell) {
		// TODO Auto-generated method stub
		return marketingNameCell.getCellType()!=1;
	}

	private boolean isInvalidManufacturer(HSSFCell manufacturerCell) {
		// TODO Auto-generated method stub
		return manufacturerCell.getCellType()!=1;
	}

	private boolean isInvalidAccessCapability(HSSFCell accessCapabilityCell) {
		// TODO Auto-generated method stub
		return accessCapabilityCell.getCellType()!=1;
	}

	private boolean isInvalidModel(HSSFCell modelCell) {
		// TODO Auto-generated method stub
		return modelCell.getCellType()!=1;
	}

	private boolean isInvalidVendorName(HSSFCell vendorNameCell) {
		// TODO Auto-generated method stub
		return vendorNameCell.getCellType()!=1;
	}

	private boolean isInvalidUeType(HSSFCell ueTypeCell) {
		// TODO Auto-generated method stub
		return ueTypeCell.getCellType()!=1;
	}

	private boolean isInvalidOs(HSSFCell osCell) {
		// TODO Auto-generated method stub
		return osCell.getCellType()!=1;
	}

	private boolean isInvalidInputMode(HSSFCell inputModeCell) {
		// TODO Auto-generated method stub
		return inputModeCell.getCellType()!=1;
	}

}
