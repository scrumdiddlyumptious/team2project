package com.team2.dataset.validator;

import org.apache.poi.hssf.usermodel.HSSFCell;

public class MCC_MNCValidator implements Validators {

	public boolean validate(HSSFCell[] dataObject) {
		boolean dataValid = true;
		for (HSSFCell dataCell : dataObject) {
			if (dataCell.toString().equals("(null)")) {
				return false;
			}

		}
		if (isInvalidMccCell(dataObject[0])) {
			dataValid = false;
		}

		if (isInvalidMncCell(dataObject[1])) {
			dataValid = false;
		}
		
		if(isValidCountry(dataObject[2])){
			dataValid=false;
		}


		if (isInvalidOperator(dataObject[3])) {
			dataValid = false;
		}
		
		return dataValid;

}

	private boolean isInvalidMccCell(HSSFCell mccCell) {
		// TODO Auto-generated method stub
		int mcc = (int) mccCell.getNumericCellValue();
		return (mcc <200||mcc>600);
	}

	private boolean isInvalidMncCell(HSSFCell mncCell) {
		// TODO Auto-generated method stub
		int mnc = (int) mncCell.getNumericCellValue();
		return (mnc<0||mnc>1000);
	}

	private boolean isValidCountry(HSSFCell countryCell) {
		// TODO Auto-generated method stub
		return  countryCell.getCellType()!=1;
	}

	private boolean isInvalidOperator(HSSFCell operatorCell) {
		// TODO Auto-generated method stub
		return operatorCell.getCellType()!=1;
	}
	
}	
