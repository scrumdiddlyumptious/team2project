package com.team2.dataset.entities;

import java.io.Serializable;

public class EventCauseKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int causeCode; // primary key
	private int eventId; // primary key

	public EventCauseKey() {
	}

	public EventCauseKey(int causeCode, int eventId) {
		this.causeCode = causeCode;
		this.eventId = eventId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + causeCode;
		result = prime * result + eventId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventCauseKey other = (EventCauseKey) obj;
		if (causeCode != other.causeCode)
			return false;
		if (eventId != other.eventId)
			return false;
		return true;
	}

}
