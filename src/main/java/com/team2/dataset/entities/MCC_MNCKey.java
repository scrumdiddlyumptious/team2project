package com.team2.dataset.entities;

import java.io.Serializable;

public class MCC_MNCKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int mobileCountryCode; // primary key
	private int mobileNetworkCode; // primary key

	public MCC_MNCKey() {
	}

	public MCC_MNCKey(int mobileCountryCode, int mobileNetworkCode) {
		this.mobileCountryCode = mobileCountryCode;
		this.mobileNetworkCode = mobileNetworkCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mobileCountryCode;
		result = prime * result + mobileNetworkCode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MCC_MNCKey other = (MCC_MNCKey) obj;
		if (mobileCountryCode != other.mobileCountryCode)
			return false;
		if (mobileNetworkCode != other.mobileNetworkCode)
			return false;
		return true;
	}
}
