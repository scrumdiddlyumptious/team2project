package com.team2.dataset.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
		@NamedQuery(name = "getAllBaseData", query = "SELECT b FROM BaseData b"),
		@NamedQuery(name = "getIMSICallFailures", query = "SELECT b.imsi, MIN(b.date), MAX(b.date), count(b), SUM(b.duration) FROM BaseData b WHERE (b.date BETWEEN :startTime AND :endTime) GROUP BY b.imsi"),
		@NamedQuery(name = "getIMSIFailures", query = "SELECT b.id, b.causeCode, b.eventId FROM BaseData b WHERE b.imsi = :imsi"),
		@NamedQuery(name = "getIMSIFailureClass", query = "SELECT b.id, b.causeCode, b.eventId FROM BaseData b WHERE b.imsi = :imsi AND b.failureClass = :failure"),
		@NamedQuery(name = "findIMSIByDateRange", query = "SELECT b.imsi, b.date, b.failureClass FROM BaseData b WHERE (b.date BETWEEN :startDate AND :endDate)"),
		@NamedQuery(name = "findIMSIWithFailure", query = "SELECT DISTINCT b.imsi FROM BaseData b ORDER BY b.imsi"),
		@NamedQuery(name = "countIMSIFailures", query = "SELECT b.imsi, count(b) FROM BaseData b WHERE (b.date BETWEEN :startDate AND :endDate) and (b.imsi = :imsi)"),
		@NamedQuery(name = "getTopTenIMSIFailures", query = "SELECT b.imsi, count(b) FROM BaseData b WHERE (b.date BETWEEN :startTime AND :endTime) group by b.imsi order by count(b) desc limit 10"),
		@NamedQuery(name = "getManufacturers", query = "SELECT DISTINCT(u.manufacturer) from UserEquipment u WHERE u.typeAllocationCode IN (SELECT b.userEquipment FROM BaseData b )"),
		@NamedQuery(name = "getMarketingName", query = "SELECT DISTINCT u.marketingName from UserEquipment u WHERE u.manufacturer=:manufacturer"),
		@NamedQuery(name = "getFailureCountByModel", query = "SELECT u.manufacturer, u.model, MIN(b.date), MAX(b.date), count(b) from BaseData b, UserEquipment u WHERE (b.date BETWEEN :startTime AND :endTime) AND (b.userEquipment = u.typeAllocationCode) and (u.model = :model) GROUP BY b.userEquipment"),
		@NamedQuery(name = "getUniqueEventId", query = "SELECT DISTINCT(b.eventId), b.userEquipment from BaseData b WHERE b.userEquipment IN (SELECT u.typeAllocationCode FROM UserEquipment u WHERE u.marketingName=:marketingName AND u.manufacturer=:manufacturer) "),
		@NamedQuery(name = "getUniqueEventIdCauseCode", query = "SELECT DISTINCT b.eventId, b.causeCode, b.userEquipment from BaseData b WHERE b.userEquipment IN (SELECT u.typeAllocationCode FROM UserEquipment u WHERE u.marketingName=:marketingName AND u.manufacturer=:manufacturer) GROUP BY b.eventId, b.causeCode"),
		@NamedQuery(name = "findTop10FailureCombos", query = "SELECT b.market, b.operator, b.cellId, COUNT(b) FROM BaseData b WHERE b.date BETWEEN :startDate AND :endDate GROUP BY b.market, b.operator, b.cellId order by count(b) desc limit 10"),
		@NamedQuery(name = "getCountUniqueEventIdCauseCode", query = "SELECT DISTINCT b.eventId, b.causeCode, count(b), b.userEquipment from BaseData b WHERE b.userEquipment IN (SELECT u.typeAllocationCode FROM UserEquipment u WHERE u.marketingName=:marketingName AND u.manufacturer=:manufacturer) GROUP BY b.eventId, b.causeCode"),
		@NamedQuery(name = "getUniqueIMSIFailureClass", query ="SELECT DISTINCT b.imsi FROM BaseData b WHERE b.failureClass = :failure"),
		@NamedQuery(name = "getIMSIFailureCauseCodes", query = "SELECT DISTINCT b.eventId, b.causeCode from BaseData b WHERE b.imsi = :imsi"),
		@NamedQuery(name = "imsiAutocomplete", query = "SELECT DISTINCT b.imsi from BaseData b WHERE b.imsi LIKE :imsi") 

})

public class BaseData implements EntityObject, Serializable {

	private static final long serialVersionUID = 6926293664527712090L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private Date date;
	private int eventId;

	private int failureClass;
	private int userEquipment;
	private int market;
	private int operator;
	private int cellId;
	private int duration;
	private int causeCode;
	private String neVersion;
	private String imsi;
	private BigInteger hier3_ID;
	private BigInteger hier32_ID;
	private BigInteger hier321_ID;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getFailureClass() {
		return failureClass;
	}

	public void setFailureClass(int failureClass) {
		this.failureClass = failureClass;
	}

	public int getUserEquipment() {
		return userEquipment;
	}

	public void setUserEquipment(int userEquipment) {
		this.userEquipment = userEquipment;
	}

	public int getMarket() {
		return market;
	}

	public void setMarket(int market) {
		this.market = market;
	}

	public int getOperator() {
		return operator;
	}

	public void setOperator(int operator) {
		this.operator = operator;
	}

	public int getCellId() {
		return cellId;
	}

	public void setCellId(int cellId) {
		this.cellId = cellId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getCauseCode() {
		return causeCode;
	}

	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}

	public String getNeVersion() {
		return neVersion;
	}

	public void setNeVersion(String neVersion) {
		this.neVersion = neVersion;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public BigInteger getHier3_ID() {
		return hier3_ID;
	}

	public void setHier3_ID(BigInteger hier3_ID) {
		this.hier3_ID = hier3_ID;
	}

	public BigInteger getHier32_ID() {
		return hier32_ID;
	}

	public void setHier32_ID(BigInteger hier32_ID) {
		this.hier32_ID = hier32_ID;
	}

	public BigInteger getHier321_ID() {
		return hier321_ID;
	}

	public void setHier321_ID(BigInteger hier321_ID) {
		this.hier321_ID = hier321_ID;
	}

	@Override
	public String toString() {
		return "BaseData [id=" + id + ", date=" + date + ", eventId=" + eventId
				+ ", failureClass=" + failureClass + ", userEquipment="
				+ userEquipment + ", market=" + market + ", operator="
				+ operator + ", cellId=" + cellId + ", duration=" + duration
				+ ", causeCode=" + causeCode + ", neVersion=" + neVersion
				+ ", imsi=" + imsi + ", hier3_ID=" + hier3_ID + ", hier32_ID="
				+ hier32_ID + ", hier321_ID=" + hier321_ID + "]";
	}
}
