package com.team2.dataset.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(EventCauseKey.class)
//@Table(name = "event_cause")
public class EventCause implements EntityObject, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1201038022214379346L;
	@Id
	private int causeCode; // primary key
	@Id
	private int eventId; // primary key

	private String description;

	public int getCauseCode() {
		return causeCode;
	}

	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "EventCause [causeCode=" + causeCode + ", eventId=" + eventId
				+ ", description=" + description + "]";
	}

}
