package com.team2.dataset.businesslogic;


import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.team2.dataset.entities.BaseData;
import com.team2.dataset.entities.EntityObject;
import com.team2.dataset.entities.EventCause;
import com.team2.dataset.entities.FailureClass;
import com.team2.dataset.entities.MCC_MNC;
import com.team2.dataset.entities.UserEquipment;

@Stateless
@LocalBean
public class DatasetDAO {

	@PersistenceContext
	private EntityManager entityManager;

	/*
	 * @SuppressWarnings("unchecked") public List<BaseData> getAllBaseData() {
	 * List<BaseData> baseDataList = entityManager.createNamedQuery(
	 * "getAllBaseData").getResultList(); return baseDataList; }
	 */

	public int getBaseDataCount() {
		Query query = entityManager.createNamedQuery("getAllBaseData");
		return query.getResultList().size();
	}

	@SuppressWarnings("unchecked")
	public List<BaseData> getAllBaseData(int start, int amount, String colName,
			String searchTerm, String sortDir) {

		String sql = "SELECT * FROM BaseData ORDER BY " + colName + " "
				+ sortDir + " limit " + start + "," + amount;
		if (!searchTerm.equals("")) {
			String globeSearch = " where (id like '%" + searchTerm + "%'"
					+ " or date like '%" + searchTerm + "%'"
					+ " or eventId like '%" + searchTerm + "%'"
					+ " or failureClass like '%" + searchTerm + "%'"
					+ " or userEquipment like '%" + searchTerm + "%'"
					+ " or market like '%" + searchTerm + "%'"
					+ " or operator like '%" + searchTerm + "%'"
					+ " or duration like '%" + searchTerm + "%'"
					+ " or causeCode like '%" + searchTerm + "%'"
					+ " or neVersion like '%" + searchTerm + "%'"
					+ " or imsi like '%" + searchTerm + "%'"
					+ " or hier3_ID like '%" + searchTerm + "%'"
					+ " or hier32_ID like '%" + searchTerm + "%'"
					+ " or hier321_ID like '%" + searchTerm + "%')";
			sql = "SELECT * FROM BaseData " + globeSearch + " ORDER BY "
					+ colName + " " + sortDir + " limit " + start + ","
					+ amount;

		}

		return entityManager.createNativeQuery(sql, BaseData.class)
				.getResultList();

	}
	
	
	@SuppressWarnings("unchecked")
	public List<BaseData> imsiAutocomplete(String imsi) {
		imsi = imsi + "%";
		Query query = entityManager.createNamedQuery("imsiAutocomplete").setParameter("imsi", imsi )
		.setMaxResults(20);
		List<BaseData> resultList = query.getResultList();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public List<BaseData> getIMSICallFailures(Date startTime, Date endTime) {
		Query query = entityManager.createNamedQuery("getIMSICallFailures");
		query.setParameter("startTime", startTime);
		query.setParameter("endTime", endTime);
		List<BaseData> resultList = query.getResultList();
		return resultList;
	}

	/* 4.1 - start query here */
	@SuppressWarnings("unchecked")
	public List<BaseData> getIMSIFailures(String imsi) {
		List<BaseData> imsiFailures = entityManager
				.createNamedQuery("getIMSIFailures").setParameter("imsi", imsi)
				.getResultList();
		return imsiFailures;
	}

	/* 4.1 - end Query here */
	
	/* 6	 */
	
	@SuppressWarnings("unchecked")
	public List<BaseData> getIMSIFailureCauseCodes(String imsi) {
		List<BaseData> imsiFailures = entityManager
				.createNamedQuery("getIMSIFailureCauseCodes").setParameter("imsi", imsi)
				.getResultList();
		return imsiFailures;
	}
	
	/* 4.2 - start query here */

	@SuppressWarnings("unchecked")
	public List<BaseData> getIMSIFailureClass(String imsi, int failure) {
		List<BaseData> imsiFailureClass = entityManager
				.createNamedQuery("getIMSIFailureClass")
				.setParameter("imsi", imsi).setParameter("failure", failure)
				.getResultList();
		return imsiFailureClass;
	}

	/* 4.2 - end Query here */

	@SuppressWarnings("unchecked")
	public List<BaseData> getIMSIByDateRange(Date startDate, Date endDate) {		
		Query query = entityManager.createNamedQuery("findIMSIByDateRange");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		List<BaseData> resultList = query.getResultList();
		return resultList;
		

	}
	
	@SuppressWarnings("unchecked")
	public List<BaseData> getTop10FailureCombos(Date startDate, Date endDate) {
	
		Query query = entityManager.createNamedQuery("findTop10FailureCombos");
		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);
		query.setMaxResults(10);

		List<BaseData> resultList = query.getResultList();
		return resultList;
		

	}
	// Chris and Keith's Query
	
	public List<BaseData> getUniqueImsiForFailureClass(int failure){
		@SuppressWarnings("unchecked")
		List<BaseData> BaseData = (List<BaseData>) entityManager.createNamedQuery("getUniqueIMSIFailureClass")
						.setParameter("failure", failure).getResultList();
		if (BaseData.size() == 0)
			return null;
		else
			return BaseData;
	}

	public List<BaseData> getIMSIFailureCount(String imsi, Date startDate,
			Date endDate) {

		@SuppressWarnings("unchecked")
		List<BaseData> BaseData = (List<BaseData>) entityManager
				.createNamedQuery("countIMSIFailures")
				.setParameter("imsi", imsi)
				.setParameter("startDate", startDate)
				.setParameter("endDate", endDate).getResultList();
		if (BaseData.size() == 0)
			return null;
		else
			return BaseData;
	}

	public List<BaseData> getIMSIWithFailures() {
		@SuppressWarnings("unchecked")
		List<BaseData> BaseData = (List<BaseData>) entityManager
				.createNamedQuery("findIMSIWithFailure").getResultList();
		if (BaseData.size() == 0)
			return null;
		else
			return BaseData;

	}

	@SuppressWarnings("unchecked")
	public List<BaseData> getTopTenIMSIFailures(Date startTime, Date endTime) {
		Query query = entityManager.createNamedQuery("getTopTenIMSIFailures");
		query.setParameter("startTime", startTime);
		query.setParameter("endTime", endTime);
		query.setMaxResults(10);
		List<BaseData> resultList = query.getResultList();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public List<BaseData> getManufacturers() {
		Query query = entityManager.createNamedQuery("getManufacturers");
		List<BaseData> manufacturerList = query.getResultList();
		return manufacturerList;
	}

	@SuppressWarnings("unchecked")
	public List<BaseData> getMarketingName(String manufacturer) {
		Query query = entityManager.createNamedQuery("getMarketingName");
		query.setParameter("manufacturer", manufacturer);
		List<BaseData> marketingNameList = query.getResultList();
		return marketingNameList;
	}

	@SuppressWarnings("unchecked")
	public List<BaseData> getFailureCountByModel(String model, Date startTime,
			Date endTime) {
		Query query = entityManager.createNamedQuery("getFailureCountByModel");
		query.setParameter("model", model);
		query.setParameter("startTime", startTime);
		query.setParameter("endTime", endTime);
		List<BaseData> resultList = query.getResultList();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public List<BaseData> getUniqueEventId(String marketingName,
			String manufacturer) {
		Query query = entityManager.createNamedQuery("getUniqueEventId");
		query.setParameter("marketingName", marketingName);
		query.setParameter("manufacturer", manufacturer);
		List<BaseData> uniqueEventId = query.getResultList();
		return uniqueEventId;
	}

	@SuppressWarnings("unchecked")
	public List<BaseData> getUniqueEventIdCauseCode(String marketingName,
			String manufacturer) {
		Query query = entityManager
				.createNamedQuery("getUniqueEventIdCauseCode");
		query.setParameter("marketingName", marketingName);
		query.setParameter("manufacturer", manufacturer);
		List<BaseData> uniqueEventIdCauseCode = query.getResultList();
		return uniqueEventIdCauseCode;
	}
	


	@SuppressWarnings("unchecked")
	public List<BaseData> getCountUniqueEventIdCauseCode(String marketingName,
			String manufacturer) {
		Query query = entityManager
				.createNamedQuery("getCountUniqueEventIdCauseCode");
		query.setParameter("marketingName", marketingName);
		query.setParameter("manufacturer", manufacturer);
		List<BaseData> countUniqueEventIdCauseCode = query.getResultList();
		return countUniqueEventIdCauseCode;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addData(EntityObject entityObject) {
		if (entityObject instanceof BaseData) {
			BaseData baseData = (BaseData) entityObject;
			entityManager.merge(baseData);
		} else if (entityObject instanceof MCC_MNC) {
			MCC_MNC mcc_mnc = (MCC_MNC) entityObject;
			entityManager.merge(mcc_mnc);
		} else if (entityObject instanceof FailureClass) {
			FailureClass failureClass = (FailureClass) entityObject;
			entityManager.merge(failureClass);
		} else if (entityObject instanceof UserEquipment) {
			UserEquipment userEquipment = (UserEquipment) entityObject;
			entityManager.merge(userEquipment);
		} else if (entityObject instanceof EventCause) {
			EventCause eventCause = (EventCause) entityObject;
			entityManager.merge(eventCause);
		}
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	
}
