package com.team2.dataset.businesslogic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import com.team2.cache.Cache;
import com.team2.dataset.entities.EntityObject;
import com.team2.dataset.validator.DataValidator;

@Stateless
@LocalBean
public class ExcelSheetReader {

	@EJB
	private DatasetDAO datasetDAO;

	// create entity from data object
	EntityObjectFactory entityObjectFactory = new EntityObjectFactory();

	public int[] readDataSheet(String fileName, String sheetName) {
		Cache.initializeCache();
		final File excelFile = new File(fileName);
		int numberOfValidRows = 0;
		int numberOfInvalidRows = 0;
		// excelSheetWriter = new InvalidBaseDataExcelSheetWriter(sheetName);

		if (!excelFile.exists()) {
			System.out.println("File not found");
		}

		try {
			FileInputStream inputStream = new FileInputStream(excelFile);
			HSSFWorkbook workBook = new HSSFWorkbook(inputStream);
			HSSFSheet workSheet = workBook.getSheet(sheetName);

			final int rowNum = workSheet.getLastRowNum();
			final int colNum = workSheet.getRow(0).getLastCellNum();

			Cache.emptyCacheList(sheetName);

			for (int row = 1; row <= rowNum; row++) {
				final HSSFRow hssfRow = workSheet.getRow(row);
				HSSFCell[] dataObject = new HSSFCell[colNum];

				for (int col = 0; col < colNum; col++) {
					final HSSFCell cell = hssfRow.getCell(col);
					dataObject[col] = cell;
				}

				// check if dataObject is valid
				boolean dataValid = DataValidator.validateData(dataObject,
						sheetName);

				if (dataValid) {
					// create entity object
					EntityObject entityObject = entityObjectFactory
							.createEntityObject(dataObject, sheetName);
					numberOfValidRows++;
					// persist object to database
					datasetDAO.addData(entityObject);
				} else if (sheetName.equals("Base Data")) {
					String error = "";
					error += dataObject[0].getDateCellValue() + "\t";
					error += dataObject[1].toString() + "\t";
					error += dataObject[2].toString() + "\t";
					error += dataObject[3].toString() + "\t";
					error += dataObject[4].toString() + "\t";
					error += dataObject[5].toString() + "\t";
					error += dataObject[6].toString() + "\t";
					error += dataObject[7].toString() + "\t";
					error += dataObject[8].toString() + "\t";
					error += dataObject[9].toString() + "\t";
					dataObject[10].setCellType(Cell.CELL_TYPE_STRING);
					dataObject[11].setCellType(Cell.CELL_TYPE_STRING);
					dataObject[12].setCellType(Cell.CELL_TYPE_STRING);
					dataObject[13].setCellType(Cell.CELL_TYPE_STRING);
					error += dataObject[10].getStringCellValue() + "\t";
					error += dataObject[11].getStringCellValue() + "\t";
					error += dataObject[12].getStringCellValue() + "\t";
					error += dataObject[13].getStringCellValue() + "\t";
					FileWriter fw = new FileWriter("invalidData.txt", true);
					fw.write(error + "\n");
					fw.close();
					numberOfInvalidRows++;
				}
			}
		} catch (Exception e) {
			if (!sheetName.equals("Base Data")) {
				Cache.initializeCache();
			}
			e.printStackTrace();
		}
		int[] countValues = { numberOfValidRows, numberOfInvalidRows };
		return countValues;
	}
}
