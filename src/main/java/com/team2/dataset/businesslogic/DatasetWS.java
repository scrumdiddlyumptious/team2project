package com.team2.dataset.businesslogic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.team2.cache.Cache;
import com.team2.dataset.entities.BaseData;

@Path("/dataset")
@Stateless
@LocalBean
public class DatasetWS {

	private String[] cols = { "id", "date", "eventId", "failureClass",
			"userEquipment", "market", "operator", "cellId", "duration",
			"causeCode", "neVersion", "imsi", "hier3_ID", "hier32_ID",
	"hier321_ID" };

	private String fileName;

	@EJB
	private ExcelSheetReader esr = new ExcelSheetReader();

	@EJB
	private DatasetDAO datasetDAO;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public BaseDataDataTable findAll(@QueryParam("sEcho") Integer sEcho,
			@QueryParam("iDisplayLength") Integer iDisplayLength,
			@QueryParam("iDisplayStart") Integer iDisplayStart,
			@QueryParam("sSearch") String search,
			@QueryParam("iSortCol_0") String iSortCol_0,
			@QueryParam("sSearch") String sSearch,
			@QueryParam("sSortDir_0") String sSortDir_0) {

		final int totalRecords = datasetDAO.getBaseDataCount();
		int col = Integer.parseInt(iSortCol_0);
		String colName = cols[col];

		final BaseDataDataTable baseDataDataTable = new BaseDataDataTable();
		final List<BaseData> baseDataList = datasetDAO.getAllBaseData(
				iDisplayStart, iDisplayLength, colName, sSearch, sSortDir_0);

		baseDataDataTable.setAaData(baseDataList);
		baseDataDataTable.setiTotalDisplayRecords(String.valueOf(totalRecords));
		baseDataDataTable.setiTotalRecords(String.valueOf(totalRecords));
		baseDataDataTable.setsEcho(sEcho);

		return baseDataDataTable;
	}


	@GET
	@Path("/imsiAutocomplete/{imsi}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getAllIMSIs(@PathParam("imsi") String imsi) {
		return datasetDAO.imsiAutocomplete(imsi);
	}

	@GET
	@Path("/imsicallfailures/{startTime}&{endTime}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getIMSICallFailures(
			@PathParam("startTime") String startTimeString,
			@PathParam("endTime") String endTimeString) throws ParseException {
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date startTimeIn = inFormat.parse(startTimeString);
		String startTimeOut = outFormat.format(startTimeIn);

		Date endTimeIn = inFormat.parse(endTimeString);
		String endTimeOut = outFormat.format(endTimeIn);

		Date startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(startTimeOut);

		Date endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(endTimeOut);
		System.out.println(startTime);

		return datasetDAO.getIMSICallFailures(startTime, endTime);
	}

	/* 4.1 - start query here */
	@GET
	@Path("/getIMSIFailures/{imsi}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getIMSIFailures(@PathParam("imsi") String imsi) {
		// BigInteger bigImsi = new BigInteger(imsi);
		return datasetDAO.getIMSIFailures(imsi);
	}

	/* 4.1 - end Query here */

	/* 6 */

	@GET
	@Path("/imsifailurecausecodes/{imsi}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getIMSIFailureCauseCodes(@PathParam("imsi") String imsi) {

		return datasetDAO.getIMSIFailureCauseCodes(imsi);
	}

	/* 4.2 - start query here */
	@GET
	@Path("/getIMSIFailureClass/{imsi}&{failureClass}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getIMSIFailureClass(
			@PathParam("imsi") String imsi,
			@PathParam("failureClass") int failure) {
		// BigInteger bigImsi = new BigInteger(imsi);
		return datasetDAO.getIMSIFailureClass(imsi, failure);
	}

	/* 4.2 - end Query here */

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/IMSIByDateRange/{startDateString}&{endDateString}")
	public List<BaseData> getIMSIByDateRange(
			@PathParam("startDateString") String startDateString,
			@PathParam("endDateString") String endDateString)	throws ParseException {

		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date startDateIn = inFormat.parse(startDateString);
		String startDateOut = outFormat.format(startDateIn);

		Date endDateIn = inFormat.parse(endDateString);
		String endDateOut = outFormat.format(endDateIn);

		Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(startDateOut);

		Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.parse(endDateOut);
		
		System.out.println(startDate);
		return datasetDAO.getIMSIByDateRange(startDate, endDate);
		
		
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/top10FailureCombos/{startDateString}&{endDateString}")
	public List<BaseData> getTop10FailureCombos(
			@PathParam("startDateString") String startDateString,
			@PathParam("endDateString") String endDateString)
					throws ParseException {

		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date startDateIn = inFormat.parse(startDateString);
		String startDateOut = outFormat.format(startDateIn);

		Date endDateIn = inFormat.parse(endDateString);
		String endDateOut = outFormat.format(endDateIn);

		Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm")
		.parse(startDateOut);

		Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm")
		.parse(endDateOut);
		return datasetDAO.getTop10FailureCombos(startDate, endDate);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/IMSIFailureCount/{imsi}&{startDateString}&{endDateString}")
	public List<BaseData> getIMSIFailureCount(
			@PathParam("imsi") String imsi,
			@PathParam("startDateString") String startDateString,
			@PathParam("endDateString") String endDateString)
					throws ParseException {

		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date startDateIn = inFormat.parse(startDateString);
		String startDateOut = outFormat.format(startDateIn);

		Date endDateIn = inFormat.parse(endDateString);
		String endDateOut = outFormat.format(endDateIn);

		Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm")
		.parse(startDateOut);

		Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm")
		.parse(endDateOut);

		return datasetDAO.getIMSIFailureCount(imsi, startDate, endDate);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/IMSIWithFailure")
	public List<BaseData> getIMSIWithFailure() {
		return datasetDAO.getIMSIWithFailures();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/TopTenIMSIFailures/{startDateString}&{endDateString}")
	public List<BaseData> getTopTenIMSIFailures(
			@PathParam("startDateString") String startDateString,
			@PathParam("endDateString") String endDateString)
					throws ParseException {

		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date startDateIn = inFormat.parse(startDateString);
		String startDateOut = outFormat.format(startDateIn);

		Date endDateIn = inFormat.parse(endDateString);
		String endDateOut = outFormat.format(endDateIn);

		Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm")
		.parse(startDateOut);

		Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm")
		.parse(endDateOut);
		return datasetDAO.getTopTenIMSIFailures(startDate, endDate);
	}

	@GET
	@Path("/getManufacturers/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getManufacturers() {
		return datasetDAO.getManufacturers();
	}

	@GET
	@Path("/getMarketingName/{manufacturer}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getMarketingName(
			@PathParam("manufacturer") String manufacturer) {
		return datasetDAO.getMarketingName(manufacturer);
	}

	@GET
	@Path("/failurecountbymodel/{manufacturer}&{model}&{startTime}&{endTime}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getFailureCountByModel(
			@PathParam("model") String model,
			@PathParam("startTime") String startTimeString,
			@PathParam("endTime") String endTimeString) throws ParseException {
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");

		SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date startDateIn = inFormat.parse(startTimeString);
		String startDateOut = outFormat.format(startDateIn);

		Date endDateIn = inFormat.parse(endTimeString);
		String endDateOut = outFormat.format(endDateIn);

		Date startDate = new SimpleDateFormat("yyyy-MM-dd hh:mm")
		.parse(startDateOut);
		Date endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm")
		.parse(endDateOut);

		return datasetDAO.getFailureCountByModel(model, startDate, endDate);
	}

	@GET
	@Path("/getUniqueEventId/{marketingName}&{manufacturer}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getUniqueEventId(
			@PathParam("marketingName") String marketingName,
			@PathParam("manufacturer") String manufacturer) {
		return datasetDAO.getUniqueEventId(marketingName, manufacturer);
	}

	@GET
	@Path("/getUniqueEventIdCauseCode/{marketingName}&{manufacturer}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getUniqueEventIdCauseCode(
			@PathParam("marketingName") String marketingName,
			@PathParam("manufacturer") String manufacturer) {
		return datasetDAO
				.getUniqueEventIdCauseCode(marketingName, manufacturer);
	}

	@GET
	@Path("/getCountUniqueEventIdCauseCode/{marketingName}&{manufacturer}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getCountUniqueEventIdCauseCode(
			@PathParam("marketingName") String marketingName,
			@PathParam("manufacturer") String manufacturer) {
		return datasetDAO.getCountUniqueEventIdCauseCode(marketingName,
				manufacturer);
	}


	//Chris and Keith Query
	@GET
	@Path("/getUniqueImsiForFailureClass/{failureClass}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BaseData> getUniqueImsiForFailureClass(
			@PathParam("failureClass") int failure) {
		// BigInteger bigImsi = new BigInteger(imsi);
		return datasetDAO.getUniqueImsiForFailureClass(failure);
	}


	@POST
	@Path("/upload")
	@Consumes("application/vnd.ms-excel")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> uploadFile(InputStream inputStream) {
		fileName = "dataset.xls";

		// set cache filename
		Cache.setFileName(fileName);

		// save file to folder
		saveFile(inputStream);

		// get sheet names from file
		List<String> sheetNames = getExcelSheetNames();
		List<String> messages = new ArrayList<String>();

		// persist data sheets
		for (String sheetName : sheetNames) {
			String message = persistDataTable(sheetName);
			messages.add(message);
		}
		return messages;
	}

	private void saveFile(InputStream uploadedInputStream) {
		try {
			OutputStream outputStream = new FileOutputStream(new File(fileName));
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = uploadedInputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
			outputStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private List<String> getExcelSheetNames() {
		List<String> sheetNames = new ArrayList<String>();

		FileInputStream fileInputStream = null;

		try {
			fileInputStream = new FileInputStream(fileName);
			HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);

			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add(0, workbook.getSheetName(i));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sheetNames;
	}

	private String persistDataTable(String tableName) {
		String message = "";

		try {
			// Read excel file and create data object
			int[] rowsEntered = esr.readDataSheet(fileName, tableName);
			message = "Successfully added " + rowsEntered[0]
					+ " valid rows to " + tableName + " (" + rowsEntered[1]
							+ " invalid rows not entered)";
		} catch (Exception e) {
			message = "Error adding table " + tableName + " to the database";
			e.printStackTrace();
		}
		return message;
	}

	public DatasetDAO getDataPersistorDAO() {
		return datasetDAO;
	}

	public void setDataPersistorDAO(DatasetDAO dataPersistorDAO) {
		this.datasetDAO = dataPersistorDAO;
	}
}
