package com.team2.dataset.businesslogic;

import java.util.List;

import com.team2.dataset.entities.BaseData;


public class BaseDataDataTable {

	private Integer sEcho;
	private String iTotalRecords;
	private String iTotalDisplayRecords;
	private List<BaseData> aaData;
	
	public String getiTotalRecords() {
		return iTotalRecords;
	}
	
	public void setiTotalRecords(String iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getsEcho() {
		return sEcho;
	}

	public void setsEcho(Integer sEcho) {
		this.sEcho = sEcho;
	}

	public String getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(String iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public List<BaseData> getAaData() {
		return aaData;
	}

	public void setAaData(List<BaseData> aaData) {
		this.aaData = aaData;
	}
	
	

}