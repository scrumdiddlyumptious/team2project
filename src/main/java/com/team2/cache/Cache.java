package com.team2.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.team2.dataset.businesslogic.EntityObjectFactory;
import com.team2.dataset.businesslogic.TableType;
import com.team2.dataset.entities.EntityObject;
import com.team2.dataset.entities.EventCause;
import com.team2.dataset.entities.FailureClass;
import com.team2.dataset.entities.MCC_MNC;
import com.team2.dataset.entities.UserEquipment;

//@Stateless
//@LocalBean
public class Cache {
	private static List<EventCause> eventCauseList = new ArrayList<EventCause>();
	private static List<FailureClass> failureClassList = new ArrayList<FailureClass>();
	private static List<UserEquipment> userEquipmentList = new ArrayList<UserEquipment>();
	private static List<MCC_MNC> mccmncList = new ArrayList<MCC_MNC>();
	
	private static String fileName = "";

	public static void setFileName(String newFileName) {
		fileName = newFileName;
	}

	public static void initializeCache() {
		EntityObjectFactory entityObjectFactory = new EntityObjectFactory();
		TableType[] metaDataTables = { TableType.EVENT_CAUSE,
				TableType.FAILURE_CLASS, TableType.MCC_MNC, TableType.UE_TABLE };
	
		//Val file path below
		//final File excelFile = new File("C:\\Users\\User\\git\\team2project\\AIT Group Project - Dataset 3A.xls");
		
		final File excelFileIn = new File(fileName);
		//Trial use of GIT
		for (int i = 0; i < metaDataTables.length; i++) {
			try {
				FileInputStream inputStream = new FileInputStream(excelFileIn);

				HSSFWorkbook workBook = new HSSFWorkbook(inputStream);
				HSSFSheet workSheet = workBook.getSheet(metaDataTables[i]
						.getTableName());

				final int rowNum = workSheet.getLastRowNum();
				final int colNum = workSheet.getRow(0).getLastCellNum();

				for (int row = 1; row <= rowNum; row++) {
					final HSSFRow hssfRow = workSheet.getRow(row);
					HSSFCell[] dataObject = new HSSFCell[colNum];

					for (int col = 0; col < colNum; col++) {
						final HSSFCell cell = hssfRow.getCell(col);
						dataObject[col] = cell;
					}
					EntityObject entityObject = entityObjectFactory
							.createEntityObject(dataObject,
									metaDataTables[i].getTableName());
					addToCache(entityObject);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	public static List<EventCause> getEventCauseList() {
		return eventCauseList;
	}

	public static List<FailureClass> getFailureClassList() {
		return failureClassList;
	}

	public static List<UserEquipment> getUserEquipmentList() {
		return userEquipmentList;
	}

	public static List<MCC_MNC> getMCCMNCList() {
		return mccmncList;
	}

	public static void addToCache(EntityObject entityObject) {

		if (entityObject instanceof EventCause) {
			EventCause eventCause = (EventCause) entityObject;
			eventCauseList.add(eventCause);
		} else if (entityObject instanceof FailureClass) {
			FailureClass failureClass = (FailureClass) entityObject;
			failureClassList.add(failureClass);
		} else if (entityObject instanceof UserEquipment) {
			UserEquipment userEquipment = (UserEquipment) entityObject;
			userEquipmentList.add(userEquipment);
		} else if (entityObject instanceof MCC_MNC) {
			MCC_MNC mcc_mnc = (MCC_MNC) entityObject;
			mccmncList.add(mcc_mnc);
		}
	}

	public static void emptyCacheList(String sheetName) {
		if (sheetName.equals(TableType.EVENT_CAUSE.getTableName())) {
			eventCauseList.clear();
		} else if (sheetName.equals(TableType.FAILURE_CLASS.getTableName())) {
			failureClassList.clear();
		} else if (sheetName.equals(TableType.MCC_MNC.getTableName())) {
			mccmncList.clear();
		} else if (sheetName.equals(TableType.UE_TABLE.getTableName())) {
			userEquipmentList.clear();
		}
	}

	public static void clear() {
		eventCauseList.clear();
		failureClassList.clear();
		userEquipmentList.clear();
		mccmncList.clear();

	}

	@SuppressWarnings("unchecked")
	private static void readFromFile() throws Exception {
		FileInputStream fin = new FileInputStream(new File("cache"));
		ObjectInputStream oin = new ObjectInputStream(fin);
		List<? extends EntityObject> cache = (List<? extends EntityObject>) oin
				.readObject();
		oin.close();

	}

	public static void writeToFile() throws Exception {
		FileOutputStream fout = new FileOutputStream(new File("cache"));
		ObjectOutputStream oout = new ObjectOutputStream(fout);

		Map<String, List<? extends EntityObject>> cache = new HashMap<String, List<? extends EntityObject>>();
		cache.put("EventCauseList", eventCauseList);
		cache.put("FailureClassList", failureClassList);
		cache.put("UserEquipmentList", userEquipmentList);
		cache.put("MCCMNC", mccmncList);

		oout.writeObject(cache);
		oout.close();
	}
}