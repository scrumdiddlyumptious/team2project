package com.team2.user.entities;

public enum UserType {
	ADMINISTRATOR("Administrator"), CUSTOMER_SERVICE_REP(
			"Customer Service Rep."), SUPPORT_ENGINEER("Support Engineer"), NETWORK_MANAGEMENT_ENGINEER(
			"Network Management Engineer");

	private String type;

	private UserType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
